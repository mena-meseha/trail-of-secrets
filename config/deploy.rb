set :stages, %w[production]
set :default_stage, 'production'

require 'capistrano/ext/multistage'

set :default_environment, {
}

def environment  
  if exists?(:stage)
    stage
  elsif exists?(:node_env)
    node_env  
  elsif ENV['NODE_ENV']
    ENV['NODE_ENV']
  else
    'production'  
  end
end

# suppress output that might contain sensitive info like passwords by setting log level > DEBUG
def discreet_output(&block)
  ll = self.logger.level
  self.logger.level = Capistrano::Logger::INFO  
  yield
  self.logger.level = ll
end

def templates_path
  expanded_path_for('deploy/templates')
end

def expanded_path_for(path)
  e = File.join(File.dirname(__FILE__),path)
  File.expand_path(e)
end

def parse_template(file)
  require 'erb'
  template = File.read(file)
  return ERB.new(template).result(binding)
end

def generate_template(local_file, remote_file)
  temp_file = '/tmp/' + File.basename(local_file)
  server_temp_file = '/tmp/capops.txt'
  
  buffer = parse_template(local_file)
  File.open(temp_file, 'w+') { |f| f << buffer }
  upload temp_file, server_temp_file, :via => :scp
  
  run "sudo mv -f #{server_temp_file} #{remote_file}"
  run_locally "rm #{temp_file}"
end

load "config/deploy/app"
load "config/deploy/scm"
load "config/deploy/tasks/ec2"
load "config/deploy/tasks/ubuntu"
load "config/deploy/tasks/logs"
load "config/deploy/tasks/haproxy"
load "config/deploy/tasks/node"
load "config/deploy/tasks/npm"
load "config/deploy/tasks/db"
load "config/deploy/tasks/ssl"

namespace :deploy do
  task :start, :roles => :app, :except => { :no_release => true } do
    run "sudo SS_ENV=#{environment} NODE_ENV=#{environment} start node"
  end

  task :stop, :roles => :app, :except => { :no_release => true } do
    run "sudo stop node"
  end

  task :restart, :roles => :app, :except => { :no_release => true } do
    run "sudo restart node || sudo SS_ENV=#{environment} NODE_ENV=#{environment} start node"
  end

  task :status, :roles => :app do
    run "status node"
  end  

  task :migrate do; end;
end

namespace :sh do
  desc "Launches SSH session"
  task :default, :roles => :app do
    system "ssh -i #{ssh_options[:keys]} #{user}@#{domain}"
  end
end