set :branch, 'master'
set :node_env, "production"

ssh_options[:keys] = "~/.ec2/keypairs/trailofsecrets.pem"

set :domain, "ec2-23-21-145-35.compute-1.amazonaws.com"
role :web, domain
role :app, domain
role :db,  domain, :primary => true