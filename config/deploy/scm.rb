  # SCM settings

  # REF: https://bitbucket.org/site/master/issue/4600/cannot-clone-a-public-wiki-over-ssh
  # set :repository, "mena.meseha@gmail.com:travis_dunn/trail-of-secrets.git"

  set :scm, :git
  set :repository, "https://mena.meseha@gmail.com/travis_dunn/trail-of-secrets.git"
  set :scm_verbose, true
  set :git_enable_submodules, 1
  set :keep_releases, 2
  set :deploy_via, :remote_cache
  set :deploy_to, "/var/www/#{application}"

  # Git settings for capistrano
  default_run_options[:pty] = true
  ssh_options[:forward_agent] = true

  after "deploy:update", "deploy:cleanup" 