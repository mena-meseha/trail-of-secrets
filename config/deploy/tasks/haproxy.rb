namespace :haproxy do
  task :start, :roles => :app do
    run "sudo start haproxy"
  end

  task :restart, :roles => :app do
    run "sudo restart haproxy || sudo start haproxy"
  end  

  task :stop, :roles => :app do
    run "sudo stop haproxy"
  end  

  task :status, :roles => :app do
    run "status haproxy"
  end    

  task :setup, :roles => :app do
    run "sudo sed 's/ENABLED\s*=\s*0/ENABLED=1/' < /etc/default/haproxy > /tmp/haproxy"
    run "sudo mv -f /tmp/haproxy /etc/default/haproxy"

    run "sudo update-rc.d -f haproxy remove"
    run "sudo update-rc.d -f networking remove"
    run "sudo update-rc.d haproxy start 37 2 3 4 5 . stop 20 0 1 6 ."
    run "sudo update-rc.d networking start 34 2 3 4 5 ."

    generate_template "#{templates_path}/haproxy.cfg.erb", "/etc/haproxy/haproxy.cfg"
  end

  desc "Build latest haproxy dev version (SSL, SPDY, etc)"
  task :make, :roles => :app do
    run "wget -O /tmp/haproxy.tar.gz http://haproxy.1wt.eu/download/1.5/src/devel/haproxy-1.5-dev16.tar.gz"
    run "cd /tmp && tar -xzf haproxy.tar.gz"
    run "cd haproxy*"
    run "make TARGET=linux2628 USE_PCRE=1 USE_OPENSSL=1 USE_ZLIB=1"
    run "sudo make install"
    run "sudo ln -nfs /usr/local/sbin/haproxy /usr/sbin/haproxy"
    run "sudo rm -rf /tmp/haproxy*"
  end

  desc "Check the haproxy config"
  task :check, :roles => :app do
    run "haproxy -f /etc/haproxy/haproxy.cfg -c"
  end

  task :version, :roles => :app do
    run "haproxy -vv"
  end  
end