set :ec2_metadata_url, "http://169.254.169.254/latest/meta-data/"

set :ami_ubuntu_quantal_ebs_us_east, "ami-9465dbfd"
set :ami_ubuntu_quantal_instance_us_east, "ami-e864da81"
set :aws_security_group, "Web Server"
set :aws_access_key, "AKIAIVNVQSADHTNWDV4A"
set :aws_secret_key, "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"

def run_ec2(cmd)
   system "#{cmd} -O #{aws_access_key} -W #{aws_secret_key}"
end

namespace :ec2 do
  desc "Write instance metadata to local text files"
  task :setup, :roles => :app do
    run "curl \"#{ec2_metadata_url}hostname\" > ~/ec2_hostname"
    run "curl \"#{ec2_metadata_url}public-hostname\" > ~/ec2_public_hostname"
    run "curl \"#{ec2_metadata_url}local-hostname\" > ~/ec2_local_hostname"
    run "curl \"#{ec2_metadata_url}ami-id\" > ~/ec2_ami_id"
    run "curl \"#{ec2_metadata_url}ami-manifest-path\" > ~/ec2_ami_manifest_path"
    run "curl \"#{ec2_metadata_url}placement/availability-zone\" > ~/ec2_az"
  end

  desc "Add swap space (NB: ONLY run on Micro instances!!!)"
  task :swap, :roles => :app do
    run "sudo dd if=/dev/zero of=/swapfile bs=1M count=1024" # (1 megabyte * 1024) swap space
    run "sudo mkswap /swapfile"
    run "sudo swapon /swapfile"
    run "sudo sed -i '$a /swapfile swap swap defaults 0 0' /etc/fstab"
  end  

  desc "Launch a new EC2 Web Server"
  task :launch do
    run_ec2 "ec2-run-instances #{ami_ubuntu_quantal_ebs_us_east} -t t1.micro -z us-east-1a -k trailofsecrets -g '#{aws_security_group}'"
  end

  desc "Describe EC2 instances"
  task :describe do
    run_ec2 "ec2-describe-instances"
  end  
end

namespace :deploy do
  desc "Deploy assets to S3 CDN"
  task :assets do
  end
end

after "deploy:setup", "ec2:setup"