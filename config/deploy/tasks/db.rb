require 'json'

def db_config
  config = ''
  db_file = File.join(shared_path, 'config', "#{environment}.json")  
  run "cat #{db_file}" do |ch, st, data| 
    config << data
  end   
  config = JSON.parse config
  config['Database']['Application']
end

set :db_name, 'trail_of_secrets'
set :local_backup_dir, '/tmp/backups/'
set :game_db_collections,  %w(users games leaderboards scores)

namespace :db do
  desc "Export production database collections and import into local development DB"
  task :export do
    backup
    game_db_collections.each do |c|
      run_locally "mongoimport --db #{db_name} --collection #{c} --file #{local_backup_dir}#{c}.json"
    end
    backup_cleanup
  end

  task :backup do
    # export from server connection
    config = db_config
    database = config['db']
    un = config['username']
    pw = config['password'] 
    host = config['host']
    port = config['port']

    db_auth = "--host #{host} --port #{port} --username #{un} --password #{pw} --db #{database}"

    discreet_output do
      game_db_collections.each do |c|
        run_locally "mongoexport #{db_auth} --collection #{c} --out #{local_backup_dir}#{c}.json"
      end
    end
  end

  task :backup_cleanup do
    run_locally "rm -rf #{local_backup_dir}"
  end
end