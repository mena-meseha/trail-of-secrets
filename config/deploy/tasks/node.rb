namespace :node do
  task :symlink, :roles => :app do
    shared_file = File.join(shared_path, 'config', "#{environment}.json")
    release_file = File.join(current_release, 'config', "#{environment}.json")
    run("ln -nfs #{shared_file} #{release_file}")    
  end

  task :setup, :roles => :app do
    run "mkdir -p #{deploy_to}"
    run "mkdir -p #{shared_path}/node_modules"
    run "mkdir -p #{shared_path}/config"
    run "sudo chown #{user}:#{user} #{deploy_to}"
    node.config
  end

  task :config, :roles => :app do
    set(:db_username) { Capistrano::CLI.ui.ask "Enter #{environment} database user name:" }
    set(:db_password) { Capistrano::CLI.password_prompt "Enter #{environment} database user password:" }
    set(:fb_app_secret) { Capistrano::CLI.password_prompt "Enter Facebook app secret:" }
    generate_template "#{templates_path}/#{environment}.json.erb", "#{shared_path}/config/#{environment}.json"
  end
end

after 'deploy:setup', 'node:setup'
after 'deploy:finalize_update', 'node:symlink'