namespace :ssl do
  task :check, :roles => :app do
    run "openssl x509 -in /etc/ssl/certs/#{domain}.pem -text -noout"
    run "openssl rsa -in /etc/ssl/private/#{domain}.key -check"
  end

  task :connect, :roles => :app do
    run "openssl s_client -connect 127.0.0.1:443 -servername #{domain}"
  end
end