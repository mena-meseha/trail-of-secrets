namespace :logs do
  desc "Tail the system log"
  task :sys, :roles => :app do
    run "#{try_sudo} tail -250 /var/log/syslog"
  end    

  desc "Download system log to local desktop"
  task :download, :roles => :app do
    download "/var/log/syslog", "~/Desktop/#{application}_syslog"
  end

  task :setup, :roles => :app do
    run "sudo apt-get install logwatch libdate-manip-perl"
    run "​sudo mkdir /var/cache/logwatch"
  end

  desc "Send Logwatch report email"
  task :email, :roles => :app do
    set(:email) { Capistrano::CLI.ui.ask "Enter email address for log report:" }
    set(:days) { Capistrano::CLI.ui.ask "Enter # of days of logs to report:" }
    run "sudo logwatch --mailto #{email} --output mail --format html --range 'between -#{days} days and today'"
  end
end