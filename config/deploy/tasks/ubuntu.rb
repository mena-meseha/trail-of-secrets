namespace :ubuntu do
  namespace :aptget do
    task :update, :roles => :app do
      run "sudo apt-get update"
    end

    task :upgrade, :roles => :app do
      run "sudo apt-get -y upgrade --show-upgraded"
    end

    task :autoremove, :roles => :app do
      run "sudo apt-get -y autoremove"
    end      
  end

  namespace :ulimit do
    task :a, :roles => :app do
      run "ulimit -a"
    end

    task :setup, :roles => :app do
      run "sudo sed -i '$a root soft nofile 51200' /etc/security/limits.conf"
      run "sudo sed -i '$a root hard nofile 51200' /etc/security/limits.conf"
      run "sudo sed -i '$a * soft nofile 51200' /etc/security/limits.conf"
      run "sudo sed -i '$a * hard nofile 51200' /etc/security/limits.conf"

      run "sudo sed -i '$a session required pam_limits.so' /etc/pam.d/common-session"

      run "sudo sed -i '$a ulimit -SHn 51200' /etc/profile"
    end
  end

  namespace :upstart do
    task :setup, :roles => :app do
      generate_template "#{templates_path}/upstart/haproxy.conf.erb", "/etc/init/haproxy.conf"
      run "sudo chmod u+x /etc/init/haproxy.conf"

      generate_template "#{templates_path}/upstart/node.conf.erb", "/etc/init/node.conf"
      run "sudo chmod u+x /etc/init/node.conf"
    end
  end

  task :timezone, :roles => :app do
    run "sudo apt-get -y install language-pack-en-base"
    run "sudo locale-gen en_GB.UTF-8"
    run "sudo /usr/sbin/update-locale LANG=en_GB.UTF-8"
  end       

  task :top_tasks, :roles => :app do
    run "top"
  end

  task :disk_free, :roles => :app do
    run "df -h"
  end

  task :netstat, :roles => :app do
    run "sudo netstat -anltp | grep 'LISTEN'"
  end  

  task :setup, :roles => :app do
    run "sudo add-apt-repository -y ppa:chris-lea/node.js"

    ubuntu.aptget.update
    ubuntu.aptget.upgrade

    run "sudo apt-get -y install build-essential g++"
    run "sudo apt-get -y install python-software-properties"
    run "sudo apt-get -y install apache2-utils"
    run "sudo apt-get -y install libpcre3 libpcre3-dev"
    run "sudo apt-get -y install libssl-dev pkg-config"
    run "sudo apt-get -y install git-core"

    run "sudo apt-get -y install haproxy" # alternatively run task haproxy.make for latest dev version 1.5-x
    run "sudo apt-get -y install nodejs npm"

    haproxy.setup
    ubuntu.upstart.setup

    run "sudo mkdir -p #{deploy_to} && sudo chown #{user}:root #{deploy_to}"
  end
end

before "deploy:setup", "ubuntu:timezone", "ubuntu:ulimit:setup", "ubuntu:setup"
after "deploy:setup", "ubuntu:upstart:setup"