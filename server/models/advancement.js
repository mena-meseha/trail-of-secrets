var mongoose = require('mongoose');
var Schema = mongoose.Schema

var advancements = {
  conflict: {_id: 'conflict', name: "Dangerous", delta: 1, chance: 2},
  research: {_id: 'research', name: "Insight", delta: 1, chance: 1},
  health: {_id: 'health', name: "Grit", delta: 3, chance: 20},
  sanity: {_id: 'sanity', name: "Sangfroid", delta: 2, chance: 15},
  occult: {_id: 'occult', name: "Occultism", delta: 1, chance: 2},
  magic: {_id: 'magic', name: "Wizardry", delta: 5, chance: 15},
  adventure: {_id: 'adventure', name: "Steadfast", delta: 1, chance: 2},
  wards: {_id: 'wards', name: "Theurgy", delta: 1, chance: 7},
  hunting: {_id: 'hunting', name: "Cryptozoology", delta: 5, chance: 8},
  healing: {_id: 'healing', name: "Medicine", delta: 5, chance: 8},
  funding: {_id: 'funding', name: "Affluence", delta: 5, chance: 8},
  augury: {_id: 'augury', name: "Mysticism", delta: 5, chance: 8},
  arcana: {_id: 'arcana', name: "Premonitions", delta: 5, chance: 8},
}
function advancementList(){
  var a = []
  for (var key in advancements) { a.push(advancements[key]) }
  return a
}

var dictionary = {
  _id: String,
  chance: Number,
  delta: Number,
  name: String
}
var Advancement = new Schema(dictionary);

Advancement.statics.menu = function () {
  function randomChance(list) {
    var total = 0
    for (var i = 0; i < list.length; i++) { total += list[i].chance}
    return Math.floor(Math.random() * total)
  }

  // Build a menu of three random, unique advancements based on their chance of appearing
  var m = []
  var a = advancementList()

  for (var i = 0; i < 3; i++) {
    var num = randomChance(a)
    console.log("ADVANCEMENT randomChance "+num+' a len='+a.length)
    var cursor = 0
    for (var j = 0; j < a.length; j++) {
      var advancement = a[j]
      if (num >= cursor && num < cursor + advancement.chance) {
        m.push(advancement)
        a.splice(j, 1)
        break
      }
      cursor += advancement.chance
    }

  }
  return m
}

module.exports.Lookups = advancements
module.exports.Advancement = Advancement
module.exports.Dictionary = dictionary