var mongoose = require('mongoose');
var Schema = mongoose.Schema

var MENU_SIZE = 3

var cast = function (val) {
  if (!val) return null;
  var I = mongoose.model('Item');
  item = new I(val)
  return item
}


// event: immediate game modifier 
// recovery: immediately replenishes stat(s)
// effect: game modifier than lasts one or more turns
// weapon/accessory/attire: adds 'base' or 'bonus' to stats

var items = {
  tourniquet: {_id: 'tourniquet', cost: 5, patronage: 1, name: 'Tourniquet', type: 'recovery', description: 'Restore 10 health!'},
  first_aid_kit: {_id: 'first_aid_kit', cost: 10, patronage: 1, name: 'First Aid Kit', type: 'recovery', description: 'Restore 20 health!'},
  blood_transfusion: {_id: 'blood_transfusion', cost: 15, patronage: 1, name: 'Blood Transfusion', type: 'recovery', description: 'Restore 50 health!'},
  elixir_of_life: {_id: 'elixir_of_life', cost: 30, patronage: 2, name: 'Elixir of Life', type: 'recovery', description: 'Restore all health!'},
  reanimation_serum: {_id: 'reanimation_serum', cost: 10, patronage: 1, name: 'Reanimation Serum', type: 'recovery', description: 'Restore all health, lose 90% sanity'},
  whisky: {_id: 'whisky', cost: 5, patronage: 1, name: 'Whisky', type: 'recovery', description: 'Restore 10 sanity!'},
  midnight_oil: {_id: 'midnight_oil', cost: 10, patronage: 1, name: 'Midnight Oil', type: 'recovery', description: 'Restores 20 sanity'},
  psychoanalysis: {_id: 'psychoanalysis', cost: 10, patronage: 1, name: 'Psychoanalysis', type: 'recovery', description: 'Restore all sanity, lose all wards and magic'},
  toadstone: {_id: 'toadstone', cost: 5, patronage: 1, name: 'Toadstone', type: 'recovery', description: 'Restore 5 wards'},
  adder_stone: {_id: 'adder_stone', cost: 10, patronage: 1, name: 'Adder Stone', type: 'recovery', description: 'Restore 15 wards'},
  abramelin_oil: {_id: 'abramelin_oil', cost: 20, patronage: 2, name: 'Abramelin Oil', type: 'recovery', description: 'Restore all wards'},
  black_lotus: {_id: 'black_lotus', cost: 10, patronage: 1, name: 'Black Lotus', type: 'recovery', description: 'Restore all magic, lose 90% sanity'},
  panchrest: {_id: 'panchrest', cost: 75, patronage: 3, name: 'Panchrest', type: 'recovery', description: 'Restores all health and sanity'},
  yithian_mind_trip: {_id: 'yithian_mind_trip', cost: 50, patronage: 5, name: 'Yithian Mind Trip', type: 'recovery', description: 'Gain 33% experience'},
  four_species: {_id: 'four_species', cost: 15, patronage: 1, name: 'Four Species', type: 'recovery', description: 'Gain health, sanity, wards, magic for each monster on the board'},

  federal_raid: {_id: 'federal_raid', cost: 20, patronage: 1, name: 'Federal Raid', type: 'event', description: 'Convert all resources to weapons'},
  hand_grenade: {_id: 'hand_grenade', cost: 10, patronage: 1, name: 'Hand Grenade', type: 'event', description: 'Damage each monster for 10'},
  hospitalization: {_id: 'hospitalization', cost: 5, patronage: 1, name: 'Hospitalization', type: 'event', description: 'Restore most health, skip 20 turns'},
  shrieking_mandrake: {_id: 'shrieking_mandrake', cost: 5, patronage: 1, name: 'Shrieking Mandrake', type: 'event', description: 'Each monster loses -1 defense'},  
  powder_of_ibn_ghazi: {_id: 'powder_of_ibn_ghazi', cost: 25, patronage: 2, name: 'Powder of Ibn-Ghazi', type: 'event', description: 'Halves monster defense and damage'},
  shamans_brew: {_id: 'shamans_brew', cost: 15, patronage: 1, name: "Shaman's Brew", type: 'event', description: 'Halves monster terror'},
  space_mead: {_id: 'space_mead', cost: 35, patronage: 3, name: 'Space Mead', type: 'event', description: 'Travel to a new board.'},

  plutonian_drug: {_id: 'plutonian_drug', cost: 5, patronage: 1, name: 'The Plutonian Drug', type: 'effect', description: 'Skip next monster turn'},
  vesta_of_liao: {_id: 'vesta_of_liao', cost: 30, patronage: 2, name: 'Vesta of Liao', type: 'effect', description: 'Collect double tiles next match'},

  almanac: {_id: 'almanac', cost: 50, patronage: 2, name: 'Almanac', type: 'accessory', modifiers: [{_id: 'research', value: 1}], description: '+1 research skill'},
  gris_gris: {_id: 'gris_gris', cost: 75, patronage: 4, name: 'Gris-gris', type: 'accessory', modifiers: [{_id: 'augury', value: 15},{_id: 'occult', value: 1}], description: '+15% augury, +1 occult skill'},
  magnifying_glass: {_id: 'magnifying_glass', cost: 75, patronage: 4, name: 'Magnifying Glass', type: 'accessory', modifiers: [{_id: 'arcana', value: 15}], description: '+15% arcana'},
  mathers_table: {_id: 'mathers_table', cost: 125, patronage: 6, name: 'Mathers Table', type: 'accessory', modifiers: [{_id: 'augury', value: 30}], description: '+30% augury'},
  sun_cross: {_id: 'sun_cross', cost: 125, patronage: 6, name: 'Sun Cross', type: 'accessory', modifiers: [{_id: 'healing', value: 20}, {_id: 'occult', value: 1}, {_id: 'augury', value: 20}], description: '+20% healing, +20% augury, + 1 occult skill'},

  workmans_coveralls: {_id: 'workmans_coveralls', cost: 50, patronage: 2, name: "Workman's Coveralls", type: 'attire', modifiers: [{_id: 'adventure', value: 1}], description: '+1 adventure skill'},
  opera_attire: {_id: 'opera_attire', cost: 75, patronage: 3, name: 'Opera Cloak and Domino Mask', type: 'attire', modifiers: [{_id: 'adventure', value: 1},{_id: 'occult', value: 1}], description: '+1 adventure and occult skill'},
  top_hat_and_tails: {_id: 'top_hat_and_tails', cost: 125, patronage: 6, name: 'Top Hat and Tails', type: 'attire', modifiers: [{_id: 'funding', value: 35}], description: '+35% funding'},
  academical_regalia: {_id: 'academical_regalia', cost: 275, patronage: 15, name: 'Academical Regalia', type: 'attire', modifiers: [{_id: 'research', value: 1}], description: '+1 research skill'},
  american_sportswear: {_id: 'american_sportswear', cost: 75, patronage: 3, name: 'American Sportswear', type: 'attire', modifiers: [{_id: 'adventure', value: 1}, {_id: 'conflict', value: 1}], description: '+1 adventure and conflict skill'},  
  frock_coat: {_id: 'frock_coat', cost: 175, patronage: 8, name: 'Frock Coat', type: 'attire', modifiers: [{_id: 'adventure', value: 2}], description: '+2 adventure skill'},  
  pinstriped_mobster_suit: {_id: 'pinstriped_mobster_suit', cost: 230, patronage: 12, name: 'Pinstriped Mobster Suit', type: 'attire', modifiers: [{_id: 'hunting', value: 25}, {_id: 'conflict', value: 1}], description: '+25% hunting, +1 conflict skill'},  
  cassock_and_collar: {_id: 'cassock_and_collar', cost: 100, patronage: 5, name: 'Cassock and Collar', type: 'attire', modifiers: [{_id: 'healing', value: 35}], description: '+35% healing'},  
  freemasons_aprons_and_jewels: {_id: 'freemasons_aprons_and_jewels', cost: 175, patronage: 8, name: "Freemason's Apron and Jewels", type: 'attire', modifiers: [{_id: 'occult', value: 3}], description: '+3 occult skill'},  
  silk_evening_wear: {_id: 'silk_evening_wear', cost: 150, patronage: 7, name: "Silk Evening Wear", type: 'attire', modifiers: [{_id: 'occult', value: 1}, {_id: 'research', value: 1}], description: '+1 occult and +1 research skill'},  
  leather_aviator_suit_and_goggles: {_id: 'leather_aviator_suit_and_goggles', cost: 175, patronage: 8, name: "Leather Aviator Suit & Goggles", type: 'attire', modifiers: [{_id: 'adventure', value: 3}], description: '+3 adventure skill'},  

  derringer: {_id: 'derringer', cost: 15, patronage: 1, name: 'Derringer', type: 'weapon', modifiers: [{_id: 'adventure', value: 1}], description: '+1 adventure skill'},
  flare_gun: {_id: 'flare_gun', cost: 20, patronage: 1, name: 'Flare Gun', type: 'weapon', modifiers: [{_id: 'hunting', value: 20}], description: '+20% hunting'},
  crossbow: {_id: 'crossbow', cost: 30, patronage: 2, name: 'Crossbow', type: 'weapon', modifiers: [{_id: 'occult', value: 2}, {_id: 'conflict', value: 1}], description: '+2 occult and +1 conflict skill'},
  carbine_rifle: {_id: 'carbine_rifle', cost: 65, patronage: 3, name: 'Carbine Rifle', type: 'weapon', modifiers: [{_id: 'conflict', value: 3}, {_id: 'adventure', value: 1}], description: '+3 conflict and +1 adventure skill'},
  colt_thirty_two_revolver: {_id: 'colt_thirty_two_revolver', cost: 40, patronage: 2, name: 'Colt .32 Revolver', type: 'weapon', modifiers: [{_id: 'conflict', value: 1}, {_id: 'adventure', value: 1}], description: '+1 conflict and adventure skill'},
  cavalry_saber: {_id: 'cavalry_saber', cost: 25, patronage: 2, name: 'Cavalry Saber', type: 'weapon', modifiers: [{_id: 'conflict', value: 1}], description: '+1 conflict skill'},
  sword_cane: {_id: 'sword_cane', cost: 40, patronage: 2, name: 'Sword Cane', type: 'weapon', modifiers: [{_id: 'conflict', value: 1}, {_id: 'occult', value: 1}], description: '+1 conflict and occult skill'},
  snubnose_thirty_eight: {_id: 'snubnose_thirty_eight', cost: 40, patronage: 2, name: 'Snubnose .38 Revolver', type: 'weapon', modifiers: [{_id: 'conflict', value: 2}], description: '+2 conflict skill'},
  three_fifty_seven_magnum:  {_id: 'three_fifty_seven_magnum', cost: 85, patronage: 3, name: '.357 Magnum', type: 'weapon', modifiers: [{_id: 'conflict', value: 5}], description: '+5 conflict skill'},
  winchester_shotgun: {_id: 'winchester_shotgun', cost: 100, patronage: 5, name: 'Winchester Shotgun', type: 'weapon', modifiers: [{_id: 'hunting', value: 50},{_id: 'conflict', value: 1}], description: '+50% hunting, +1 conflict skill'},
  sawed_off_shotgun: {_id: 'sawed_off_shotgun', cost: 120, patronage: 6, name: 'Sawed Off Shotgun', type: 'weapon', modifiers: [{_id: 'hunting', value: 20}, {_id: 'conflict', value: 4}], description: '+20% hunting, +4 conflict skill'},
  three_o_three_lee_enfield: {_id: 'three_o_three_lee_enfield', cost: 160, patronage: 8, name: '.303 Lee-Enfield', type: 'weapon', modifiers: [{_id: 'hunting', value: 15}, {_id: 'conflict', value: 2}, {_id: 'adventure', value: 2}], description: '+15% hunting, +2 adventure and conflict skill'},
  elephant_gun: {_id: 'elephant_gun', cost: 275, patronage: 13, name: 'Elephant Gun', type: 'weapon', modifiers: [{_id: 'conflict', value: 9}], description: '+9 conflict skill'},
  thompson_submachine_gun: {_id: 'thompson_submachine_gun', cost: 200, patronage: 10, name: 'Thompson Submachine Gun', type: 'weapon', modifiers: [{_id: 'hunting', value: 65}, {_id: 'conflict', value: 2}], description: '+65% hunting, +2 conflict skill'},
  mp_18_submachine_gun: {_id: 'mp_18_submachine_gun', cost: 250, patronage: 13, name: 'MP 18 Submachine Gun', type: 'weapon', modifiers: [{_id: 'hunting', value: 75}, {_id: 'conflict', value: 4}], description: '+75% hunting, +4 conflict skill'},
  forty_five_automatic: {_id: 'forty_five_automatic', cost: 70, patronage: 3, name: '.45 Automatic', type: 'weapon', modifiers: [{_id: 'hunting', value: 45}], description: '+45% hunting'},
  forty_four_magnum: {_id: 'forty_four_magnum', cost: 85, patronage: 3, name: '.44 Magnum', type: 'weapon', modifiers: [{_id: 'conflict', value: 4}], description: '+4 conflict skill'},
  mondragon_automatic_rifle: {_id: 'mondragon_automatic_rifle', cost: 125, patronage: 6, name: 'Mondragon Automatic Rifle', type: 'weapon', modifiers: [{_id: 'conflict', value: 2}, {_id: 'adventure', value: 3}], description: '+3 adventure and +2 conflict skill'},
  browning_automatic_rifle: {_id: 'browning_automatic_rifle', cost: 150, patronage: 7, name: '.45 Automatic', type: 'weapon', modifiers: [{_id: 'conflict', value: 3}, {_id: 'weapons', value: 25}, {_id: 'adventure', value: 1}], description: '+25% hunting, +3 conflict and +1 adventure skill'},
  flamethrower: {_id: 'flamethrower', cost: 250, patronage: 13, name: 'Flamethrower', type: 'weapon', modifiers: [{_id: 'weapons', value: 55},{_id: 'adventure', value: 2}], description: '+55% hunting, +2 adventure skill'}
}
function itemList(){
  var a = []
  for (var key in items) { 
    var item = items[key]
    a.push(item) 
  }
  return a
}

// premium items are normalized in facebook credits and localized client-side (e.g. 10 fbc = $1)
var premium = {
  unexpected_inheritance: {_id: 'unexpected_inheritance', cost: 10, name: 'Unexpected Inheritance', type: 'currency', description: 'gain +10 patronage'},
  mysterious_benefactor: {_id: 'mysterious_benefactor', cost: 30, name: 'Mysterious Benefactor', type: 'currency', description: 'gain +50 patronage'},
  masonic_fellowship: {_id: 'masonic_fellowship', cost: 50, name: 'Masonic Fellowship', type: 'currency', description: 'gain +100 patronage'},
  wilmarth_foundation_endowment: {_id: 'wilmarth_foundation_endowment', cost: 200, name: 'Wilmarth Foundation Endowment', type: 'currency', description: 'gain +1000 patronage'},
}
function premiumList(){
  var a = []
  for (var key in premium) { 
    var item = premium[key]
    a.push(item) 
  }
  return a
}

// tikkoun exlixir

var modifier = {
  _id: String,
  value: Number
}
var Modifier = new Schema(modifier);

var dictionary = {
  _id: String,
  description: String,
  sold: {type: Boolean, default: false},
  cost: Number,
  patronage: Number,
  modifiers: [Modifier],
  name: String,
  type: String
}
var Item = new Schema(dictionary);


Item.statics.premiumShop = premiumList
Item.statics.supplies = function (investigator) {
  var list = itemList()
  var s = []

  // add 1 equipment item to menu, but ensure investigator does not already own it
  var items = []
  var I = mongoose.model('Item')
  for (var i = list.length - 1; i >= 0; i--) {
    var item = new I(list[i])
    if (item.isEquipment()) {
      if (!investigator || !investigator.hasEquipment(item._id)) items.push(item)
      list.splice(i, 1)
    }
  }
  s.push( items[ Math.floor(Math.random() * items.length) ])

  var insanity = investigator.insanity('stimga') // a stimga increases the cost of buying supplies
  var fee = insanity ? (insanity.level * 0.30) : 0

  var I = mongoose.model('Item')
  // add MENU_SIZE more consumable items to menu
  for (var i = 0; i < MENU_SIZE; i++) {
    var index = Math.floor(Math.random() * list.length)
    var item = new I(list[index])
    if (fee > 0) item.cost += Math.ceil(fee * item.cost)

    s.push( item.serialize() )
    list.splice(index, 1)
  }
  return s
}
Item.statics.lookup = function (itemId) {
  var list = itemList().concat( premiumList() )
  for (var i = list.length - 1; i >= 0; i--) {
    if (list[i]._id === itemId) {
      var I = mongoose.model('Item')
      return new I(list[i])
    }
  }
}

Item.methods.sell = function (investigatorOrUser, usePatronage) {
  if (usePatronage) {
    investigatorOrUser.modifyPatronage(-this.patronage)
  } else {
    investigatorOrUser.modifyWealth(-this.cost)
  }
  this.sold = true
  return this.cost
}
Item.methods.isEquipment = function () {
  return this.type === 'accessory' || this.type === 'attire' || this.type === 'weapon'
}
Item.methods.equip = function (investigator) {
  if (!this.isEquipment()) return

  for (var i = 0; i < investigator.equipment.length; i++) {
    if (investigator.equipment[i].type === this.type) {
      investigator.equipment.splice(i, 1)
      break
    }
  }

  investigator.equipment.push( this.serializeCompact() )
}
Item.methods.purchase = function (user) {
  if (this.type !== 'currency') return
 
  switch (this._id) {
    case 'unexpected_inheritance':
      user.patronage += 10
      break;
    case 'mysterious_benefactor':
      user.patronage += 50
      break;
    case 'masonic_fellowship':
      user.patronage += 100
      break;
    case 'wilmarth_foundation_endowment':
      user.patronage += 1000
      break;
  }
}
Item.methods.modifier = function (modifierId) {
  var mod = 0
  for (var i = 0; i < this.modifiers.length; i++) { 
    var isIdMatch = this.modifiers[i]._id === modifierId
    if (isIdMatch) mod += this.modifiers[i].value
  }
  return mod
}
Item.methods.serializeCompact = function () {
  var serialObject = {
    _id: this._id, 
    name: this.name, 
    type: this.type, 
    description: this.description
  }
  if (this.modifiers && this.modifiers.length) serialObject.modifiers = this.modifiers
  return serialObject
}
Item.methods.serialize = function () {
  var serialObject = {
    _id: this._id,
    cost: this.cost,
    patronage: this.patronage,
    description: this.description,
    name: this.name,
    type: this.type
  }
  if (this.modifiers && this.modifiers.length) serialObject.modifiers = this.modifiers
  return serialObject
}

module.exports.Item = Item
module.exports.Dictionary = dictionary
module.exports.Cast = cast