var mongoose = require('mongoose');
var Schema = mongoose.Schema

var cast = function (val) {
  if (!val) return null;
  var M = mongoose.model('Monster');
  monster = new M(val)
  return monster
}
var monsters = {
  cultist: {_id: 'cultist', name: 'Cultist', health: 2, damage: 1, terror: 0, defense: 1, turn: 0, chance: 75, experience: 1, description: "A weak enemy with no special ability of their own, cultists are often used or enhanced by other monsters."},
  high_priest: {_id: 'high_priest', name: 'High Priest', health: 5, damage: 1, terror: 1, defense: 2, turn: 15, chance: 25, experience: 2, traits: ['inspire'], description: "Inspiring their followers, if a high priest is on the board then all cultists double their damage and defense."},
  chosen_one: {_id: 'chosen_one', name: 'Chosen One', health: 5, damage: 3, terror: 1, defense: 3, turn: 30, chance: 10, experience: 1, traits: ['possession'], description: "A diabolic enemy that possesses cultists, if a chosen one is killed and there are any cultists on the board at the end of the next turn then it will posses a new one."},
  ghoul: {_id: 'ghoul', name: 'Ghoul', health: 3, damage: 2, terror: 2, defense: 2, turn: 40, chance: 20, experience: 3, traits: ['necrophage'], description: "The necrophagic ghoul eats the dead and permanently gains 1 damage every time another monster dies"},
  ghost: {_id: 'ghost', name: 'Ghost', health: 2, damage: 1, terror: 3, defense: 4, turn: 40, chance: 15, experience: 3, traits: ['phantasmal'], description: "Ghosts are condemned to a phantasmal curse of recurring power and pain, and cannot be damaged on odd-numbered turns."},
  werewolf: {_id: 'werewolf', name: 'Werewolf', health: 5, damage: 1, terror: 1, defense: 4, turn: 50, chance: 15, experience: 4, traits: ['lunar'], description: "The werewolf is empowered by the lunar cycle and on every fifth turn doubles its terror and quintuples its damage."},
  ghast: {_id: 'ghast', name: 'Ghast', health: 8, damage: 2, terror: 1, defense: 2, turn: 60, chance: 12, experience: 5, traits: ['pack'], description: "Ghasts are murderous pack hunters and deal 1 additional damage for every other non-ghast monster on the board."},  
  byakhee: {_id: 'byakhee', name: 'Byakhee', health: 5, damage: 3, terror: 2, defense: 3, turn: 75, chance: 11, experience: 5, traits: ['teleportation'], description: "The byakhee travels through space, exchanging its tile with another random, non-monster tile at the end of every turn."},
  nightgaunt: {_id: 'nightgaunt', name: 'Nightgaunt', health: 5, damage: 2, terror: 4, defense: 3, turn: 75, chance: 11, experience: 5, traits: ['flying'], description: "The flying nightgaunt menaces its victim from above, and cannot be damaged by matches including tiles equal to or below its position on the board."},  
  zombie: {_id: 'zombie', name: 'Zombie', health: 8, damage: 2, terror: 1, defense: 1, turn: 80, chance: 17, experience: 3, traits: ['infectous'], description: "Zombies are infectious monsters that eat and infect the dead, immediately raising other non-zombie monsters that are killed as new zombies."},
  yithian: {_id: 'yithian', name: 'Yithian', health: 10, damage: 0, terror: 0, defense: 0, turn: 100, chance: 8, experience: 3, traits: ['doppleganger'], description: "Yithians are the enemy within, alien dopplegangers who take your form with a defense, damage, and terror-to-occult equal to your own!"},
  mi_go: {_id: 'mi_go', name: 'Mi-Go', health: 5, damage: 1, terror: 2, defense: 5, turn: 100, chance: 9, experience: 5, traits: ['experiments'], description: "The mi-go are an alien race concealing intrigue and harvesting the resources of man, and each mi-go on the board will transform one clue tile into funds at the end of every turn."},
  spider_of_leng: {_id: 'spider_of_leng', name: 'Spider of Leng', health: 10, damage: 3, terror: 1, defense: 2, turn: 120, chance: 8, experience: 6, traits: ['webline'], description: "The spiders of leng descend upon weblines spanning their entire column on the board and no other tiles that are matched on that column can be collected."},
  deep_one: {_id: 'deep_one', name: 'Deep One', health: 6, damage: 2, terror: 2, defense: 3, turn: 130, chance: 15, experience: 5, traits: ['abysmal'],  description: "The abysmal deep one become stronger in descent, and permanently gain 1 damage for each row on the board it crosses including the first."},
  serpent_people: {_id: 'serpent_people', name: 'Reptilian', health: 4, damage: 3, terror: 1, defense: 3, turn: 140, chance: 10, experience: 2, traits: ['slithering'], description: "The reptilian serpent people slither their way across the board, advancing one tile at the end of every turn, from left to right and back, top to bottom, until reaching the southeast corner."},
  witch: {_id: 'witch', name: 'Witch', health: 6, damage: 2, terror: 4, defense: 3, turn: 145, chance: 11, experience: 7, traits: ['cursed'], description: "The witch's curse always breaks an additional number of wards equal to their damage when they attack."},
  wizard: {_id: 'wizard', name: 'Wizard', health: 9, damage: 4, terror: 2, defense: 6, turn: 160, chance: 7, experience: 8, traits: ['black_magic'], description: "A wizard dominates the energy around, preventing matched magic tiles from being collected and, when it attacks, draining magic equal to its damage."},
  dimensional_shambler: {_id: 'dimensional_shambler', name: 'Dimensional Shambler', health: 12, damage: 3, terror: 3, defense: 3, turn: 150, chance: 5, experience: 5, traits: ['foremost'], description: "Dimensional shamblers are eager and foremost to engage, and other monsters matched with them cannot be damaged."},
  elder_thing: {_id: 'elder_thing', name: 'Elder Thing', health: 15, damage: 2, terror: 2, defense: 2, turn: 190, chance: 5, experience: 5, traits: ['enigmatic'], description: "Elder things are enigmatic alien minds of sinister complexity, and each elder thing on the board raises the minimum number of tiles required to match for collection by 1."},
  nosferatu: {_id: 'nosferatu', name: 'Nosferatu', health: 15, damage: 5, terror: 3, defense: 5, turn: 200, chance: 6, experience: 9, traits: ['vampiric'], description: "The vampiric nosferatu restore their own health equal to the damage they deal after each attack."},
  tcho_tcho: {_id: 'tcho_tcho', name: 'Tcho-Tcho', health: 5, damage: 3, terror: 2, defense: 6, turn: 225, chance: 5, experience: 10, traits: ['aftermost'], description: "The elusive Tcho-Tcho cannot be damaged unless they are the only monster on the board."},
  rat_thing: {_id: 'rat_thing', name: 'Rat-Thing', health: 10, damage: 2, terror: 5, defense: 6, turn: 240, chance: 3, experience: 12, traits: ['familiar'], description: "The sadistic familiar rat things entrap you in magic, and each rat thing on the board will transform one weapon tile into a rite at the end of every turn."},
  shantak: {_id: 'shantak', name: 'Shantak', health: 10, damage: 3, terror: 1, defense: 3, turn: 250, chance: 5, experience: 7, traits: ['gliding'], description: "The shantak swoops upon its prey, and cannot be damaged by matches including tiles above its position on the board."},  
  gnoph_keh: {_id: 'gnoph_keh', name: 'Gnoph-Keh', health: 10, damage: 4, terror: 2, defense: 4, turn: 265, chance: 5, experience: 8, traits: ['boreal'], description: "The Gnoph-Keh is a boreal monstrosity freezing its surroundings and preventing any of the adjacent tiles included in a match from being collected."},
  fire_vampire: {_id: 'fire_vampire', name: 'Fire Vampire', health: 10, damage: 5, terror: 2, defense: 4, turn: 275, chance: 5, experience: 10, traits: ['burning'] ,description: "When matched in an attack fire vampires immediately unleash an infero dealing an equal amount of damage as they receive to you and any other monsters in the match."},
  star_spawn: {_id: 'star_spawn', name: 'Star Spawn', health: 25, damage: 8, terror: 6, defense: 6, turn: 290, chance: 3, experience: 15, traits: ['worship'], description: "Worshipped by the occult hordes, when Star Spawn appear on the board they turn all other new, non-monster tiles into cultists."},
  dark_young: {_id: 'dark_young', name: 'Dark Young', health: 12, damage: 6, terror: 3, defense: 3, turn: 315, chance: 4, experience: 10, traits: ['shadowy'], description: "Stalking through the shadows of space, dark young can only be damaged when they are on odd rows of the board."},
  lloigor: {_id: 'lloigor', name: 'Lloigor', health: 15, damage: 4, terror: 1, defense: 2, turn: 330, chance: 3, experience: 8, traits: ['immaterial'], description: "The astral form of the lloigor makes it immune to attacks from the mundane, and you cannot damage it unless your magic is greater than 0."},
  colour_out_of_space: {_id: 'colour_out_of_space', name: 'The Colour Out of Space', health: 25, damage: 0, terror: 2, defense: 5, turn: 345, chance: 2, experience: 15, traits: ['contamination'], description: "The colour out of space contaminates its surroundings, and while it is on the board no matched recovery or resource tiles can be collected."},
  dhole: {_id: 'dhole', name: 'Dhole', health: 35, damage: 7, terror: 4, defense: 4, turn: 375, chance: 4, experience: 15, traits: ['tunneling'], description: "The dhole burrows through the bowels of the earth, and at the end of each turn will tunnel from its current location to the bottommost tile on the board below."},
  liche: {_id: 'liche', name: 'Liche', health: 20, damage: 4, terror: 2, defense: 5, turn: 385, chance: 5, experience: 15, traits: ['immortal'], description: "The liche is an immortal whose soul is locked inside a phylactery; when the liche is killed it transforms into a phylactery which can be matched with resources for one turn, after which it transforms back into a liche."},
  hound_of_tindalos: {_id: 'hound_of_tindalos', name: 'Hound of Tindalos', health: 10, damage: 5, terror: 5, defense: 6, turn: 400, chance: 3, experience: 15, traits: ['angles'], description: "The hounds of Tindalos stalk euclidian angles, and can only be damaged by matches including tiles in a straight line."},
  formless_spawn: {_id: 'formless_spawn', name: 'Formless Spawn', health: 5, damage: 2, terror: 1, defense: 7, turn: 450, chance: 2, experience: 5, traits: ['consumption'], description: "The formless spawn slowly consumes all matter, and each spawn will randomly transform another non-monster tile into a new formless spawn at the end of every turn."},
  shoggoth: {_id: 'shoggoth', name: 'Shoggoth', health: 50, damage: 5, terror: 3, defense: 5, turn: 100, chance: 1, experience: 475, traits: ['menace'], description: "Vanguards of the great old ones, for each shoggoth on the board the number of seals required in a match is increased by 1."}
}

var dictionary = {
  _id: String,
  chance: Number,
  description: String,
  name: String,
  health: Number,
  damage: Number,
  defense: Number,
  experience: Number,
  terror: Number,
  traits: [String],
  turn: Number
}
var Monster = new Schema(dictionary);

Monster.statics.lookups = function () {
  return monsters
}

Monster.methods.isDead = function () {
  return this.health === 0
}
Monster.methods.serialize = function () {
  var serialObject = {
    _id: this._id, 
    name: this.name, 
    health: this.health, 
    life: monsters[this._id].health,
    damage: this.damage, 
    defense: this.defense, 
    terror: this.terror 
  }
  return serialObject
}

module.exports.Lookups = monsters
module.exports.Monster = Monster
module.exports.Dictionary = dictionary
module.exports.Cast = cast