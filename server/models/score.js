var mongoose = require('mongoose');
var Schema = mongoose.Schema

var dictionary = {
  created: { type: Date, default: Date.now },
  gameId: String,
  difficulty: Number,
  kills: Number,
  experience: Number,
  level: Number,
  matches: Number,
  ranked: Boolean,
  score: Number,
  turns: Number,
  userId: String,
  wealth: Number
}
var Score = new Schema(dictionary);

Score.index({"userId": 1})
Score.index({"score": -1})
Score.index({"created": -1})

Score.statics.record = function (game, cb) {
  var S = mongoose.model('Score')
  var score = new S()

  score.gameId = game._id
  score.userId = game.userId
  score.kills = game.stats.monsters
  score.level = game.investigator.level
  score.experience = game.stats.experience
  score.wealth = game.stats.funds
  score.turns = game.stats.moves
  score.matches = game.stats.funds+game.stats.health+game.stats.sanity+game.stats.magic+game.stats.weapons+game.stats.seals
  score.score = game.score
  score.difficulty = game.greatOldOne().difficulty
  score.save(cb)
}
Score.statics.updateUserScores = function (user, score, cb) {
  if (user.highScore) {
    if (score.kills > user.highScore.kills) user.highScore.kills = score.kills
    if (score.level > user.highScore.level) user.highScore.level = score.level
    if (score.matches > user.highScore.matches) user.highScore.matches = score.matches
    if (score.score > user.highScore.score) user.highScore.score = score.score
    if (score.turns > user.highScore.turns) user.highScore.turns = score.turns
    if (score.wealth > user.highScore.wealth) user.highScore.wealth = score.wealth
  } else {
    user.highScore = {
      kills: score.kills,
      experience: score.experience,
      level: score.level,
      matches: score.matches,
      score: score.score,
      turns: score.turns,
      wealth: score.wealth
    }
  }
  user.save(function(err) {
    if (typeof cb === 'function') cb(err, score, user.highScore)
  })
}
Score.statics.recent = function (sortField, limit) {
  limit = limit || 25
  var yesterday = new Date();
  yesterday.setDate(yesterday.getDate()-1);

  var filter = { created: {$gte: yesterday }}
  var sort = {}
  sort[sortField] = 'desc'

  return this.find(filter).sort(sort).limit(limit)
}

module.exports.Score = Score
module.exports.Dictionary = dictionary