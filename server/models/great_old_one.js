var mongoose = require('mongoose');
var Schema = mongoose.Schema

var great_old_ones = {
  azathoth: {_id: 'azathoth', name: "Azathoth", difficulty: 1},
  dagon: {_id: 'dagon', name: "Dagon", difficulty: 2},
  ithaqua: {_id: 'ithaqua', name: "Ithaqua", difficulty: 3},
  glaaki: {_id: 'glaaki', name: "Glaaki", difficulty: 4},
  chaugnar_faugn: {_id: 'chaugnar_faugn', name: "Chaugnar Faugn", difficulty: 5},
  shudde_mell: {_id: 'shudde_mell', name: "Shudde M'ell", difficulty: 6},
  shub_niggurath: {_id: 'shub_niggurath', name: "Shub Niggurath", difficulty: 7},
  tsathoggua: {_id: 'tsathoggua', name: "Tsathoggua", difficulty: 8},
  yog_sothoth: {_id: 'yog_sothoth', name: "Yog-Sothoth", difficulty: 9},
  hastur: {_id: 'hastur', name: "Hastur", difficulty: 10},
  nyarlathotep: {_id: 'nyarlathotep', name: "Nyarlathotep", difficulty: 11},
  cthulhu: {_id: 'cthulhu', name: "Cthulhu", difficulty: 12}
}

var monster = {
  _id: String,
  change: Number
}

var dictionary = {
  _id: String,
  difficulty: Number,
  seals: Number,
  name: String,
  monsters: [monster]
}
var GreatOldOne = new Schema(dictionary);

GreatOldOne.statics.menu = function (investigator) {
  var a = []
  for (var key in great_old_ones) { 
    var goo = great_old_ones[key]
    if (goo.difficulty <= investigator.difficulty+1) {
      a.push(goo) 
    }
  }
  return a
}
GreatOldOne.statics.lookup = function (investigator, id) {
  if (!investigator) return 'azathoth'

  var menu = this.menu(investigator)
  for (var i = 0; i < menu.length; i++) {
    if (menu[i]._id === id) return id
  }
  return 'azathoth'
}

module.exports.Lookups = great_old_ones
module.exports.GreatOldOne = GreatOldOne
module.exports.Dictionary = dictionary