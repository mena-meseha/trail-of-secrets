var mongoose = require('mongoose');
var Schema = mongoose.Schema

var insanities = {
  addiction: {_id: 'addiction', name: 'Addiction', description: 'Less wealth collected from matches'},
  amnesia: {_id: 'amnesia', name: 'Amnesia', description: 'Longer chains needed for bonus match'},
  depression: {_id: 'depression', name: 'Depression', description: 'Less experience earned for matches'},
  hallucinations: {_id: 'hallucinations', name: 'Hallucinations', description: 'Increases madness loss' },
  stimga: {_id: 'stimga', name: 'Stigma', description: 'Increases cost of buying supplies' },
  horrors: {_id: 'horrors', name: '(The) Horrors', description: 'Reduces occult resistence to sanity loss' }
}

var dictionary = {
  _id: String,
  description: String,
  level: {type: Number, default: 1},
  name: String
}
var Insanity = new Schema(dictionary);

Insanity.methods.severity = function () {
  switch (this.level) {
    case 1:
      return 'Mild'
    case 2:
      return 'Moderate'
    default:
      return 'Severe'
  }
}
Insanity.methods.serialize = function () {
  var serialObject = {
    _id: this._id,
    description: this.description,
    level: this.level, 
    name: this.severity()+' '+this.name
  }
  return serialObject
}

module.exports.Insanity = Insanity
module.exports.Dictionary = dictionary
module.exports.Lookups = insanities