var CONFIG = require('config');
var dealer = require('./../mechanics/dealer')
var mongoose = require('mongoose');
var Cell = require('./cell').Cell;
var Item = require('./item').Item;
var Investigator = mongoose.model('Investigator');
var investigator = require('./investigator');
var great_old_ones = require('./great_old_one').Lookups;
var GOO = mongoose.model('GreatOldOne');
var User = mongoose.model('User');
var Score = mongoose.model('Score');
var Schema = mongoose.Schema

var initializeSupplies = function initializeSupplies(next) {
  if (this.supplies === null || this.supplies.length === 0) {
    this.resupply()
  }
  next()
}

var initializeSeals = function initializeSeals(next) {
  if (this.seals.total === 0) {
    this.seals.total = CONFIG.Difficulty.baseSeals + 
                       (this.greatOldOne().difficulty * CONFIG.Difficulty.sealsPerLeveL)
  }
  next()
}

var initializeBoard = function initializeBoard(next) {
  if (!this.cells.length) {
    this.cells = dealer.newGameboard(this)
  }
  next()
}

var initializeInvestigator = function initializeInvestigator(next) {
  if (!this.investigator) {
    this.investigator = new Investigator()
  } else if (this.turn() === 1) {
    this.investigator.health.current = this.investigator.health.total
    this.investigator.sanity.current = this.investigator.sanity.total
    this.investigator.magic.current = 0
    this.investigator.wards.current = 0
    this.investigator.wealth = 0
  }
  next()
}

var stats = {
  clues: {type: Number, default: 0},
  experience: {type: Number, default: 0},
  spent: {type: Number, default: 0},
  funds: {type: Number, default: 0},
  health: {type: Number, default: 0},
  level: {type: Number, default: 1},
  levels: {type: Number, default: 0},
  longness: {type: Number, default: 0},
  madness: {type: Number, default: 0},
  magic: {type: Number, default: 0},
  monsters: {type: Number, default: 0},
  moves: {type: Number, default: 0},
  plots: {type: Number, default: 0},
  powers: {type: Number, default: 0},
  sanity: {type: Number, default: 0},
  seals: {type: Number, default: 0},
  weapons: {type: Number, default: 0}
}

var seals = {
  current: {type: Number, default: 0},
  total: {type: Number, default: 0}
}

var dictionary = {
  active: { type: Boolean, default: true },
  advancements: Array,
  created: { type: Date, default: Date.now },
  effects: Array,
  gooId: {type: String, default: 'azathoth'},
  investigator: { type: investigator.Dictionary, get: investigator.Cast},
  cells: [Cell],
  score: Number,
  seals: seals,
  stats: stats,
  supplies: [Item],
  updated: { type: Date, default: Date.now },
  userId: String
}
var Game = new Schema(dictionary);

Game.index({"investigator._id": 1})

Game.pre('save', initializeInvestigator)
Game.pre('save', initializeSupplies)
Game.pre('save', initializeSeals)
Game.pre('save', initializeBoard)

Game.statics.findPlayable = function (gameId, cb) {
  this.findOne({ _id: gameId}, function(err, game) {
    if (game && game.isInvestigatorDead()) {
      cb('dead')
    } else if (game && game.needsAdvancing()) {  
      cb('advancing', game)
    } else if (game && game.isSealed()) {
      cb('sealed', game)
    } else {
      cb(null, game)
    }
  })
}

Game.methods.greatOldOne = function () {
  return great_old_ones[this.gooId] || {}
}
Game.methods.indexFromCoordinates = function (x, y) {
  return (y * 7) + x
}
Game.methods.hasMonster = function (monsterId) {
  for (var i = 0; i < this.cells.length; i++) { 
    var cell = this.cells[i]
    if (cell.hasMonster() && cell.monster._id === monsterId) return true
  }
  return false
}
Game.methods.monsterCount = function (monsterId) {
  return this.cells.filter(function(c){ 
    return c.hasMonster() && c.monster._id === monsterId 
  }).size
}
Game.methods.areCellsAdjacent = function (first, second) {
  var adjacentX = Math.abs(first.x() - second.x()) === 1
  var adjacentY = Math.abs(first.y() - second.y()) === 1
  var sameY = first.y() === second.y()
  var sameX = first.x() === second.x()

  return adjacentX && (sameY || adjacentY) ||
         adjacentY && (sameX || adjacentX)
}
Game.methods.phylacteryCells = function () {
  return this.cells.filter(function(c){ return c.tileId === 'phylactery' })
}
Game.methods.clueCells = function () {
  return this.cells.filter(function(c){ return c.tileId === 'clue' })
}
Game.methods.weaponCells = function () {
  return this.cells.filter(function(c){ return c.tileType() === 'combat' && c.tileId !== 'monster' })
}
Game.methods.monsterCells = function () {
  var a = []
  for (var i = 0; i < this.cells.length; i++) { 
    if (this.cells[i].hasMonster()) a.push(this.cells[i]) 
  }
  return a
}
Game.methods.nonMonsterCells = function () {
  var a = []
  for (var i = 0; i < this.cells.length; i++) { 
    if (!this.cells[i].hasMonster()) a.push(this.cells[i]) 
  }
  return a
}
Game.methods.needsAdvancing = function () {
  return this.advancements && this.advancements.length > 0 ? true : false
}
Game.methods.isInvestigatorDead = function () {
  return this.investigator.health.current === 0
}
Game.methods.isSealed = function () {
  return this.seals.current === this.seals.total
}
Game.methods.isActive = function () {
  return !this.isSealed() && !this.needsAdvancing()
}
Game.methods.isFinished = function () {
  return this.isInvestigatorDead() || (this.isSealed() && !this.needsAdvancing())
}
Game.methods.turn = function () {
  return this.stats.moves + 1
}
Game.methods.addEffect = function (name, value) {
  value = value || true
  if (!this.hasEffect(name, value)) {
    this.effects.push([name, value])
  }
}
Game.methods.hasEffect = function (name, value) {
  for (var i = 0; i < this.effects.length; i++) {
    var effect = this.effects[i]
    if (effect[0] === name && (!value || effect[1] === value)) return true
  }
  return false
}
Game.methods.transpireEffects = function () {
  this.effects = []
}
Game.methods.swapCells = function (firstIndex, secondIndex) {
  var id = this.cells[firstIndex].tileId
  var monster = this.cells[firstIndex].monster
  var plot = this.cells[firstIndex].plot

  this.cells[firstIndex].tileId = this.cells[secondIndex].tileId
  this.cells[firstIndex].plot = this.cells[secondIndex].plot
  this.cells[firstIndex].monster = this.cells[secondIndex].monster  

  this.cells[secondIndex].tileId = id
  this.cells[secondIndex].plot = plot
  this.cells[secondIndex].monster = monster
}
Game.methods.resupply = function () {
  this.supplies = mongoose.model('Item').supplies(this.investigator)
}
Game.methods.addExperience = function (delta) {
  this.stats.experience += delta
  var investigator = this.investigator
  investigator.experience += delta
  this.investigator = investigator
}
Game.methods.useSeals = function (delta) {
  this.seals.current = Math.max( Math.min(this.seals.current + delta, this.seals.total), 0)
}
Game.methods.serialize = function () {
  var serialObject = {
    _id: this._id,
    investigator: this.investigator.serialize(), 
    cells: [],
    seals: this.seals,
    supplies: this.supplies,
    turn: this.turn()
  }
  if (this.effects) serialObject.effects = this.effects
  if (this.advancements && this.advancements.length > 0) serialObject.advancements = this.advancements
  for (var i=0; i<this.cells.length;i++) { 
  	serialObject.cells.push(this.cells[i].serialize())
  }
  return serialObject
}
Game.methods.serializeEndGame = function () {
  var serialObject = {
    _id: this._id,
    advancements: this.advancements,
    gameMenu: GOO.menu(this.investigator),
    seals: this.seals
  }
  return serialObject
}
Game.methods.recordScore = function (cb) {
  // create new score for the game
  Score.record(this, function(err, doc) {
    if (this.userId) {
      // update player's topscores
      User.findOne({ _id: this.userId}, function(err, user) {
        if (doc) Score.updateUserScores(user, doc, cb)
      })
    } else {
      cb(err, doc)
    }
  })
}

module.exports.Game = Game
module.exports.Dictionary = dictionary