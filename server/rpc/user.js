var mongoose = require('mongoose');
var User = mongoose.model('User');
var facebook = require('./../mechanics/facebook')

exports.actions = function(req, res, ss) {
  req.use('session');

  return {
    info: function(auth) {
      var userId = req.session.userId
      if (userId) {
        User.findOne({ _id: userId}, function(err, user) {
          res(err, user ? user.serialize() : null)
        })
      } else {
        res('no user id in session')
      }
    },
    update: function(auth) {
      var facebookId = req.session.facebookId
      var signedRequest = auth.facebook.signedRequest
      var payload = facebook.verifySignature(signedRequest)

      if (facebookId && payload) {
        User.findFacebook(facebookId, function(err, user) {
          if (user && !user.facebook.fullName) {
            user.name = payload.first_name
            user.facebook.fullName = payload.name
            user.facebook.username = payload.username
            user.save()
          }
          res(err)
        })
      } else {
        res('missing user id or signed request payload')
      }
    }
  }
}