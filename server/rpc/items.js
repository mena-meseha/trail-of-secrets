var mongoose = require('mongoose');
var supplies = require('./../mechanics/supplies')
var premiumShop = mongoose.model('Item').premiumShop()

exports.actions = function(req, res, ss) {
  req.use('session')
  req.use('gameFinder.loadActive')

  function respondSupplied(err, results) {
    if (err) {
      res(err)
    } else {
      req.game.save(function(err) {
        res(err, results) 
      })
    }
  }

  return {
    supply: function(auth, itemId, usePatronage) {
      supplies.supply(req.game, itemId, usePatronage, respondSupplied)
    },
    premiumShop: function(auth) {
      res(null, premiumShop)
    }
  }
}