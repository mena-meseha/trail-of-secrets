var CONFIG = require('config');
var mongoose = require('mongoose');
var Game = mongoose.model('Game');
var User = mongoose.model('User');
var GOO = mongoose.model('GreatOldOne');
var dealer = require('./../mechanics/dealer')
var monsters = require('./../models/monster').Lookups;

exports.actions = function(req, res, ss) {
  req.use('session');
  req.use('userAuth.authorizeFacebook')

  function newGame(investigator, gooId) {
    var opts = {
      investigator: investigator, 
      userId: req.session.userId,
      gooId: GOO.lookup(investigator, gooId)
    }
    var game = new Game(opts)

    game.save(function(err) {
      req.session.gameId = game._id;

      function saveAndRespond() {
        req.session.save(function(err) {
          res(err, game.serialize(), req.user)
        })
      }

      // if user exists then associate them with new game
      if (game.userId) {
        User.setUserGame(game.userId, game._id, saveAndRespond)
      } else {
        saveAndRespond()
      }
    })
  }

  return {
    restart: function() {
      newGame()
    },
    start: function(opts) {
      var gooId = opts.goo

      var gameId = req.session.gameId
      console.log("game start with sid: "+req.session.id+', gameId: '+gameId)
      if (gameId) {
        Game.findOne({ _id: gameId}, function(err, game) {
          if (!game) {
            newGame()
          } else if (game.isInvestigatorDead()) {
            newGame()
          } else if (game.needsAdvancing()) {
            if (game.isSealed()) {
              res(err, game.serializeEndGame(), req.user)
            } else {
              res(err, game.serialize(), req.user)
            }
          } else if (game.isFinished()) {
            if (gooId) {
              newGame(game.investigator, gooId)
            } else {
              res(err, game.serializeEndGame(), req.user)
            }
          } else {
            // if game was started before authorization then associate game with current user 
            var needsUserAssociation = req.session.userId && game.userId !== req.session.userId
            if (needsUserAssociation) {
              game.userId = req.session.userId
              game.save(function(err) {
                res(err, game.serialize(), req.user)
              })
            } else {
              // general case
              res(err, game.serialize(), req.user)
            }
          }
        })
      } else {
        newGame()
      }
    }
  };
}