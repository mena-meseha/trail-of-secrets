var mythos = require('./mythos')
var monsters = require('./../models/monster').Lookups;
var logger = require('./../logger')
var MATCH_LENGTH = 3

function isRecoveryBlocked(game) {
  return game.hasMonster('colour_out_of_space')
}
function isResourceBlocked(game) {
  return game.hasMonster('colour_out_of_space')
}
function isInvestigationBlocked(game) {
  return game.hasMonster('shoggoth')
}
function isMagicBlocked(game) {
  return game.hasMonster('wizard')
}
function isCollectionBlocked(game, chain) {
  var cnt = game.monsterCount('elder_thing')
  return chain.length >= 3+cnt
}
function isMatchingDouble(game) {
  // investigator collects double tiles if they used the vesta of liao
  return game.hasEffect('vesta_of_liao')
}

function isObliqueMatch(game, cell, chain) {
  for (var i = 0; i < chain.length; i++) { 
    var index = chain[i]
    var chainCell = game.cells[index]
    if (i > 0 && (chainCell.y() !== cell.y() && chainCell.x() !== cell.x())) {
      return true
    }
  }  
  return false
}

function isChainAboveCell(game, cell, chain) {
  for (var i = 0; i < chain.length; i++) { 
    var index = chain[i]
    if (index === cell._id) continue

    var chainCell = game.cells[index]
    if (chainCell.y() >= cell.y()) return false
  }
  return true
}
function isChainBelowCell(game, cell, chain) {
  for (var i = 0; i < chain.length; i++) { 
    var index = chain[i]
    if (index === cell._id) continue

    var chainCell = game.cells[index]
    if (chainCell.y() < cell.y()) return false
  }
  return true
}

function filterFrozen(game, chain) {
  var monsterCells = game.monsterCells()

  // gnoph-keh prevent collection of adjacent non-monster cells
  var gnophKeh = monsterCells.filter(function(c) { return c.monster._id === 'gnoph_keh' })
  if (gnophKeh.length > 0) {
    for (var i = chain.length; i >= 0; i--) {
      var cell = game.cells[ chain[i] ]
      for (var j = 0; j < gnophKeh.length; j++) {
        if (!cell.hasMonster() && gnophKeh[j].isAdjacentTo(cell)) chain.splice(i, 0)
      }
    }
  }

  // spiders of leng prevent collection of column neighbors
  var lengSpiders = monsterCells.filter(function(c) { return c.monster._id === 'spider_of_leng' })
  if (lengSpiders.length > 0) {
    for (var i = chain.length; i >= 0; i--) {
      var cell = game.cells[ chain[i] ]
      for (var j = 0; j < lengSpiders.length; j++) {
        if (!cell.hasMonster() && lengSpiders[j].y === cell.y) chain.splice(i, 0)
      }
    }
  }
}

function experienceFromMatches(count, multiplier) {
  var baseChance = count > MATCH_LENGTH ? Math.pow(count - MATCH_LENGTH, 2) : 0
  multiplier = multiplier || 1
  baseChance = Math.floor(baseChance / multiplier)
  return baseChance
}

function experienceMultiplier(investigator) {
  var insanity = investigator.insanity('depression') // depression reduces experience gained
  if (!insanity) return 1
  return insanity.level + 1
}

function gainMatchExperience(game, chainLength, investigator) {
  var expBonus = experienceMultiplier(investigator)
  game.addExperience( experienceFromMatches(chainLength, expBonus) )
}

exports.bonusTiles = function(game, matchCount, type) {
  var investigator = game.investigator
  var bonusTiles = 0

  var baseChance = investigator.skills[type]
  var chanceModifier = investigator.modifier(type)
  var chances = investigator.skills.research + investigator.modifier('research')
  var chance = baseChance + chanceModifier

  // insight (via chant of thoth) doubles bonus title chance
  if (game.hasEffect('insight')) chance = chance * 2

  var insanity = investigator.insanity('amnesia') // amnesia reduces chance of bonus tiles
  if (insanity) {
    baseChance = Math.ceil(baseChance / (insanity.level+1))
  }

  for (i = 0; i < matchCount; i++) { 
    for (j = 0; j < chances; j++) { 
      var num = Math.floor(Math.random() * 100)
      if (num <= baseChance) bonusTiles++
    }
  }
  if (isMatchingDouble(game)) bonusTiles += matchCount // doubles collected tiles
  return bonusTiles
}

exports.isValidTypeMatch = function(game, chain) {
  var type = game.cells[chain[0]].tileType()
  for (i = 1; i < chain.length; i++) {
  	if (game.cells[chain[i]].tileType() !== type) return false
  }
  return true;
}
exports.isValidAdjacencyMatch = function(game, chain) {
  var origin = game.cells[chain[0]]
  for (i = 1; i < chain.length; i++) { 
  	var cell = game.cells[chain[i]]
  	if (!origin.isAdjacentTo(cell)) return false
    origin = cell
  }
  return true
}

exports.chainById = function(game, chain, tileId) {
  return chain.filter(function (index) {
  	return game.cells[index].tileId === tileId
  })
}

exports.matchRecovery = function(game, turn, chain) {
  if (isRecoveryBlocked(game)) return null

  var investigator = game.investigator
  var health = exports.chainById(game, chain, 'bandages').length
  var morphine = exports.chainById(game, chain, 'morphine').length
  var bonusHealth = exports.bonusTiles(game, health+morphine, 'healing')

  var sanity = exports.chainById(game, chain, 'tincture').length
  var laudanum = exports.chainById(game, chain, 'laudanum').length
  var bonusSanity = exports.bonusTiles(game, sanity+laudanum, 'healing')

  // only gain experience during turn
  if (turn) {
    turn.bonus(bonusHealth+bonusSanity)
    gainMatchExperience(game, chain.length, investigator)
  }
  game.stats.health += health+morphine
  game.stats.sanity += sanity+laudanum

  health = health+bonusHealth
  sanity = sanity+bonusSanity
  if (morphine > 0) health = health + (morphine * 5) // morphine shots are worth five bandages
  if (laudanum > 0) sanity = sanity + (laudanum * 5) // laudanum bottles are worth five tinctures

  if (health > 0) {
    var delta = investigator.modifyHealth(health)
    if (turn && delta > 0) turn.health(delta)
  }
  if (sanity > 0) {
    var delta = investigator.modifySanity(sanity)
    if (turn && delta > 0) turn.sanity(delta)
  }

  game.investigator = investigator
}
exports.matchCombat = function(game, turn, chain) {
  var investigator = game.investigator
  var guns = exports.chainById(game, chain, 'firearm').length
  var explosives = exports.chainById(game, chain, 'dynamite').length
  var bonusWeapons = exports.bonusTiles(game, guns+explosives, 'hunting')

  gainMatchExperience(game, guns+explosives, investigator)
  game.stats.weapons += guns+explosives

  // calculate damage to monsters
  var explosiveDamage = explosives * 5
  var investigatorDamage = game.investigator.damage(guns+explosives)
  var damage = guns + explosiveDamage + bonusWeapons + investigatorDamage

  var monsterCells = null
  var hasZombies = mythos.hasZombies(game.cells)
  var hasShamblers = mythos.hasShamblers(game.cells, chain)
  var hasFireVampires = mythos.hasFireVampires(game.cells, chain)

  for (var i = 0; i < chain.length; i++) { 
    var cell = game.cells[chain[i]]

    if (cell.hasMonster()) {
      if (hasShamblers && cell.monster._id !== 'dimensional_shambler') {
        continue // when dimensional shambler is in a chain only it can be damaged
      } else if (cell.monster._id === 'hound_of_tindalos') {
        // hounds of tindalos can only be hit by straight lines
        if (isObliqueMatch(game, cell, chain)) continue
      } else if (cell.monster._id === 'nightgaunt') {
        // nightgaunts cannot be damaged by chains with tiles lower or equal to the nightgaunt's y-coordinate
        if (!isChainAboveCell(game, cell, chain)) continue
      } else if (cell.monster._id === 'shantak') {
        // shantaks cannot be damaged by chains with tiles higher than the shantak's y-coordinate
        if (!isChainBelowCell(game, cell, chain)) continue          
      } else if (cell.monster._id === 'lloigor') {          
        // lloigor cannot be damaged unless investigator has magic
        if (investigator.magic.current < 1) continue
      }

      // fire vampires add their damage to any other monsters in the chain
      var bonusDamage = (hasFireVampires && cell.monster._id !== 'fire_vampire') ? monsters.fire_vampire.damage : 0

      monsterCells = monsterCells || game.monsterCells()
      var damageDealt = cell.attackMonster(monsterCells, damage+bonusDamage, game.turn())

      if (cell.monster.isDead()) {
        var exp = monsters[cell.monster._id].experience
        game.addExperience(exp) // earn exp per monster kill
        game.stats.monsters++
        turn.kill(cell._id, cell.monster._id)

        if (cell.monster._id === 'liche') {
          // when liches die they transform into a phylactery
          mythos.phylactery(game, turn, cell)
        } else if (cell.monster._id === 'chosen_one') {
          // when chosen ones die they possess the board and turn into a cultist
          mythos.humanize(game, turn, cell)
          game.addEffect('possession')
        } else if (hasZombies) { 
          mythos.zombify(game, turn, cell)
        }
      } else {
        turn.wound(cell._id, cell.monster.health)
      }

      // fire vampires deal back equal amount of damage received
      if (damageDealt > 0 && cell.monster._id === 'fire_vampire') {
        investigator.modifyHealth(-damageDealt)
        turn.damage(damageDealt)
      }
    }
  }
}
exports.matchResources = function(game, turn, chain) {
  if (isResourceBlocked(game)) return null 

  var investigator = game.investigator
  var funds = exports.chainById(game, chain, 'funds').length
  var artifacts = exports.chainById(game, chain, 'artifact').length
  var bonusResources = exports.bonusTiles(game, funds+artifacts, 'funding')

  gainMatchExperience(game, chain.length, investigator)
  turn.bonus(bonusResources)
  game.stats.funds += funds+artifacts

  var artifactValue = artifacts * 5 // artifacts are worth 5 funds
  if (artifacts > 0) funds = funds * artifactValue  // artifacts also multiply the value of resources x 5

  funds += bonusResources

  // addiction reduces the funds gained by 20% per level
  var insanity = investigator.insanity('addiction')
  if (insanity) funds = Math.max(funds - ((insanity.level * 0.2) * funds), 0)

  investigator.modifyWealth(funds)
  turn.wealth(funds)

  game.investigator = investigator
}
exports.matchMagic = function(game, turn, chain) {
  if (isMagicBlocked(game)) return null 

  var investigator = game.investigator
  var rites = exports.chainById(game, chain, 'rite').length
  var grimoires = exports.chainById(game, chain, 'grimoire').length
  var bonusMagic = exports.bonusTiles(game, rites+grimoires, 'augury')

  if (turn) turn.bonus(bonusMagic)
  gainMatchExperience(game, chain.length, investigator)
  game.stats.magic += rites+grimoires
  
  if (grimoires > 0) rites = rites + (grimoires * 5) // grimoires are worth five rites
  var results = investigator.gainRites(rites+bonusMagic)
  game.investigator = investigator

  if (turn) {
    turn.ward(results[0])
    if (results[1] > 0) turn.magic(results[1])
  }
  game.investigator = investigator
}
exports.matchInvestigation = function(game, turn, chain) {
  if (isInvestigationBlocked(game)) return null

  var investigator = game.investigator
  var clues = exports.chainById(game, chain, 'clue').length
  var bonusClues = exports.bonusTiles(game, clues, 'arcana')

  turn.bonus(bonusClues)
  game.stats.clues += clues

  var plots = exports.chainById(game, chain, 'plot')
  for (i = 0; i < plots.length; i++) { 
    var cell = game.cells[plots[i]]
    cell.solvePlot(clues+bonusClues)
    
    if (cell.plot.isSolved()) {
      game.stats.plots++
      game.addExperience( cell.plot.experience() ) // gain exp per plot solved
      turn.solve(cell._id)
    } else {
      turn.investigate(cell._id, cell.plot.clues)
    }
  }

  gainMatchExperience(game, chain.length, investigator)
  game.investigator = investigator
}
exports.matchSeals = function(game, turn, chain) {
  var investigator = game.investigator

  var minimumSealMatch = 2 // first two matches in seal chain are ignored

  // shoggoths increase the number of seals required to match
  var shoggothCount = game.cells.filter(function(c){ return c.hasMonster() && c.monster._id === 'shoggoth' }).length
  minimumSealMatch += shoggothCount

  var seals = chain.length - minimumSealMatch

  game.useSeals(seals)
  turn.seal(seals)

  gainMatchExperience(game, chain.length, investigator)
  game.stats.seals += seals
  game.investigator = investigator
}

exports.clearMatches = function(game, chain) {
  for (i = 0; i < chain.length; i++) {
  	game.cells[chain[i]].clear()
  }
}

exports.applyMatches = function(game, turn, chain) {
  var matchType = game.cells[chain[0]].tileType()

  // update stats with new longest chain match
  if (chain.length > game.stats.longness) game.stats.longness = chain.length

  // ignore collection logic if collections aren't permitted
  if (isCollectionBlocked(game, chain)) return null

  var matchChain = chain.slice() // copy chain
  filterFrozen(game, matchChain)

  switch (matchType) {
    case 'recovery':
      exports.matchRecovery(game, turn, matchChain)
      break
    case 'combat':
      exports.matchCombat(game, turn, matchChain)
      break
    case 'resource':
      exports.matchResources(game, turn, matchChain)
      break
    case 'magic':
      exports.matchMagic(game, turn, matchChain)
      break
    case 'investigation':
      exports.matchInvestigation(game, turn, matchChain)
      break
    case 'seal':
      exports.matchSeals(game, turn, matchChain)
      break
  }
}

exports.match = function(game, turn, chain) {
  console.log('matching '+JSON.stringify(chain))

  if (chain.length < MATCH_LENGTH) return
  if (!exports.isValidTypeMatch(game, chain)) return
  if (!exports.isValidAdjacencyMatch(game, chain)) return

  exports.applyMatches(game, turn, chain)
  exports.clearMatches(game, chain)
}