var CONFIG = require('config');
var mongoose = require('mongoose');
var Insanity = mongoose.model('Insanity');
var insanities = require('./../models/insanity').Lookups;
var monsters = require('./../models/monster').Lookups;

function hasHighPriest(cells) {
  return cells.filter(function(c) { 
    return c.monster && c.monster._id === 'high_priest' 
  }).length > 0
}

exports.hasZombies = function(cells) {
  return cells.filter(function(c) { 
    return c.monster && !c.monster.isDead() && c.monster._id === 'zombie' 
  }).length > 0
}
exports.hasShamblers = function(cells, chain) {
  return chain.filter(function(i) { 
    return cells[i].hasMonster() && cells[i].monster._id === 'dimensional_shambler' 
  }).length > 0
}
exports.hasFireVampires = function(cells, chain) {
  return chain.filter(function(i) { 
    return cells[i].hasMonster() && cells[i].monster._id === 'fire_vampire'
  }).length > 0
}

function randomEmptyCell(game) {
  var cells = game.cells.filter(function(cell){ 
    return !(cell.hasMonster() && cell.monster._id === 'byakhee') 
  })

  if (cells.length > 0) {
    var index = Math.floor(Math.random() * cells.length)
    return cells[index]
  }
}

exports.phylactery = function(game, turn, cell) {
  if (cell.tileId === 'phylactery') {
    cell.tileId = 'monster'
    cell.monster = monsters['liche']
    turn.transform(cell._id, {monster: cell.monster.serialize()})
  } else if (cell.monster && cell.monster._id === 'liche') {
    cell.tileId = 'phylactery'
    cell.monster = null
    turn.unkill(cell._id) // remove monster from kill list so that tile is ignored by reseating
    turn.transform(cell._id, cell.serialize())
  }
}

exports.humanize = function(game, turn, cell) {
  if (!cell.hasMonster()) return

  cell.monster = monsters['cultist']
  turn.unkill(cell._id) // remove monster from kill list so that tile is ignored by reseating
  turn.transform(cell._id, {monster: cell.monster.serialize()})
}
exports.possess = function(game, turn, cell) {
  if (!cell.hasMonster()) return

  cell.monster = monsters['chosen_one']
  turn.transform(cell._id, {monster: cell.monster.serialize()})
}

exports.zombify = function(game, turn, cell) {
  if (!exports.hasZombies(game.cells)) return
  if (!cell.hasMonster() || !cell.monster.isDead()) return
  if (cell.monster._id === 'zombie') return

  cell.monster = monsters['zombie']
  turn.unkill(cell._id) // remove monster from kill list so that tile is ignored by reseating
  turn.transform(cell._id, {monster: cell.monster.serialize()})
}

function necrophagy(cell, turn) {
  // ghouls grow stronger when other monsters die
  if (cell.monster._id === 'ghoul') {
    var ghoul = monsters['ghoul']
    var monster = cell.monster
    monster.damage += 1
    monster.defense += 1
    cell.monster = monster

    turn.transform(cell._id, {damage: monster.damage, defense: monster.defense, health: ghoul.health})
  }
}

exports.monsterDamageBonus = function(level) {
  return Math.floor(level / 10)
}

exports.monsterTerrorBonus = function(level) {
  return Math.floor(level / 10)
}

exports.wardBreaks = function(game, cells, damage, bonus) {
  if (game.investigator.wards.current === 0) return 0
  var breaks = bonus || 0

  // if no damage caused, each monster has a chance of breaking (based on game difficulty). if damage is caused, each point has a chance of breaking
  var baseDifficulty = game.greatOldOne().difficulty - 1
  var breakChance = CONFIG.Difficulty.baseWardBreakChange + (baseDifficulty * CONFIG.Difficulty.wardBreakChange)
  
  var possible = damage > 0 ? damage : cells.length
  for (var i = 0; i < possible; i++) {
    var num = Math.floor(Math.random() * 100)
    if (num >= breakChance) breaks++
  }
  return breaks
}

exports.madden = function(investigator) {
  var possible = []
  for (var key in insanities) {
    var insanity = new Insanity(insanities[key])
    possible.push( insanity )
  }
  var num = Math.floor(Math.random() * possible.length)
  var insanity = possible[num]

  // sanity is restored after going insane
  investigator.sanity.current = investigator.sanity.total

  // increase the severity level if already possessed
  var alreadyInsane = false
  for (var i = 0; i < investigator.insanities.length; i++) {
   if (insanity._id ===  investigator.insanities[i]._id) {
    investigator.insanities[i].level = Math.min(investigator.insanities[i].level + 1, 3)
    alreadyInsane = true
    return investigator.insanities[i].serialize()
   }
  }

  // add the instanity if investigator does not already have it
  investigator.insanities.push( insanity )
  return insanity.serialize()
}

exports.activity = function(game, turn) {
  var monsterCells = game.monsterCells()

  for (var i = 0; i < monsterCells.length; i++) {
    var cell = monsterCells[i]
    if (cell.monster._id === 'byakhee') {
      // byakhees will teleport any other cell than other byakhees
      var cell = randomEmptyCell(game)
      if (cell) {
        var fromIndex = monsterCells[i]._id
        var toIndex = cell._id

        turn.jumpTile(fromIndex, toIndex)
        game.swapCells(fromIndex, toIndex)
      }
    } else if (cell.monster._id === 'yithian') {
      // yithians inherit current defense from investigator wards
      var currentDefense = game.investigator.wards.current * 2
      if (cell.monster.defense !== currentDefense) {
        monster = cell.monster
        monster.defense = currentDefense
        monster.damage = investigator.skills.conflict
        monster.terror = investigator.skills.occult

        turn.alter(cell._id, {defense: currentDefense, damage: monster.damage, terror: monster.terror})
        game.cells[cell._id].monster = monster
      }
    } else if (cell.monster._id === 'formless_spawn') {
      // formless spawn transform other (non-monster) cells into other formless spawn
      var cells = game.nonMonsterCells()
      if (cells.length > 0) {
        var index = Math.floor(Math.random() * cells.length)
        var consumed = cells[index]

        game.cells[consumed._id].monster = monsters['formless_spawn']
        game.cells[consumed._id].tileId = 'monster'
        turn.alter(consumed._id, {monster: game.cells[consumed._id].monster.serialize()})
      }
    } else if (cell.monster._id === 'mi_go') {
      // mi-go turn clue tiles into resources
      var cells = game.clueCells()
      if (cells.length > 0) {
        var index = Math.floor(Math.random() * cells.length)
        var cell = cells[index]
        game.cells[cell._id].tileId = 'funds'
        turn.alterTile(cell._id, 'funds')
      }
    } else if (cell.monster._id === 'rat_thing') {
      // rat things turn weapons into rites, starting with any weapons that are adjacent to them
      var cells = game.weaponCells()
      console.log('rat thing activated against weapons '+cells.length)
      if (cells.length > 0) {
        var weaponAdjacent = false
        for (var w = 0; w < cells.length; w++) {
          var weapon = cells[w]
          if (game.areCellsAdjacent(cell, weapon)) {
            game.cells[weapon._id].tileId = 'rite'
            turn.alterTile(weapon._id, 'rite')
            weaponAdjacent = true
            break
          }
        }
        console.log('weaponAdjacent? '+weaponAdjacent)
        if (!weaponAdjacent) {
          var index = Math.floor(Math.random() * cells.length)
          var weapon = cells[index]
          game.cells[weapon._id].tileId = 'rite'
          turn.alterTile(weapon._id, 'rite')
        }
      }
    } else if (cell.monster._id === 'wizard' && turn.attack.drains > 0) {
      turn.alter(cell._id, {health: cell.monster.life})
    } else if (cell.monster._id === 'dhole' && cell.y() < 6 ) {
      // dholes always try to tunnel to the lowest unoccupied cell
      var cells = game.nonMonsterCells()
      var bottomY = cells[cells.length-1].y()
      console.log(cells[0].y()+' - '+cells[cells.length-1].y())
      cells = cells.filter(function (c) { return c.y() === bottomY })
      var index = Math.floor(Math.random() * cells.length)
      game.swapCells(cell._id, cells[index]._id)
      turn.tunnel(cell._id, cells[index]._id)
    } else if (cell.monster._id === 'serpent_people' && cell.index < game.cells.length-1) {
      // the serpent people tunnel to the last cell on the board
      

      var nextIndex = null
      if (cell.isEvenRow() && cell.x() === 6) {
        nextIndex = cell.index + 7
      } else if (cell.isEvenRow()) {
        nextIndex = cell.index + 1
      } else if (cell.x() === 0) {
        nextIndex = cell.index + 7
      } else {
        nextIndex = cell.index - 1
      }
      console.log("found serpent people moving to idx "+nextIndex) 

      // serpent people won't displace other serpent people
      if (!game.cells[nextIndex].hasMonster('serpent_people')) {
        game.swapCells(cell._id, nextIndex)
        turn.tunnel(cell._id, nextIndex)
      }
    }
  }
}

exports.update = function(game, turn) {
  // check for post-death monster abilities
  if (turn.kills.length > 0) {
    var monsterCells = game.monsterCells()

    for (var i = 0; i < monsterCells.length; i++) {
      var cell = monsterCells[i]
      if (cell.monster.isDead()) continue

      // ghouls eat the dead for strength
      necrophagy(cell, turn)
    }

    for (var i = 0; i < turn.kills.length; i++) {
      if (turn.kills[i].monsterId === 'mi_go') {
        // dead mi-go turn into artifacts
        turn.alterTile(turn.kills[i]._id, 'artifact')
      }
    }
  }

  // check for phylactery transformations
  var phylacterys = game.phylacteryCells()
  for (var i = 0; i < phylacterys.length; i++) {
    var cell = phylacterys[i]
    var isNewPhylactery = turn.transforms.filter(function(c){ return c._id === cell._id }).length === 0
    if (!isNewPhylactery) {
      exports.phylactery(game, turn, cell)
    }
  }

  // check for chosen one possessions of cultists
  if (game.hasEffect('possession')) {
    var monsterCells = game.monsterCells()
    var cultists = monsterCells.filter(function(c){ return c.monster._id === 'cultist' })
    if (cultists.length > 1) {
      var cell = cultists[ Math.floor(Math.random() * cultists.length) ]
      exports.possess(game, turn, cell)
    }
  }
}

exports.attack = function(game, turn) {
  var investigator = game.investigator
  var cells = game.cells.filter(function (cell) {
  	return cell.hasMonster()
  })

  // create lookup table if there are any active bindings (stops monster from attacking)
  var bindings = game.hasEffect('binding')
  if (bindings) {
    bindings = []
    for (var i = 0; i < game.effects.length; i++) {
      if (game.effects[i][0] === 'binding') bindings.push(game.effects[i][1])
    }
  }

  // caclulate total monster damage and terror
  var monsterCells = game.monsterCells()
  var damage = 0
  var terror = 0
  var breaks = 0
  var drains = 0
  for (var i = 0; i < cells.length; i++) {
    var cell = cells[i]

    // skip attack if monster bound
    if (bindings) {
      console.log("BINDINGS! "+JSON.stringify(bindings)+' -- '+(bindings[0] === true))
      if (bindings[0] === true) continue
      var isMonsterBound = bindings.some(function(b){ return b[1] === cell._id })
      if (isMonsterBound) continue
    }

    damage += cell.monster.damage
    terror += cell.monster.terror

    if (cell.monster._id === 'cultist' && hasHighPriest(monsterCells)) {
      // high priests double cultist damage 
      damage += cell.monster.damage // add damage again (i.e. x2)
    } else if (cell.monster._id === 'werewolf' && game.turn() % 5 === 0) {
      // werewolves deal x5 damage and x2 terror on full moons (every 5 turns)
      damage += (cell.monster.damage * 4)
      terror += cell.monster.terror
    } else if (cell.monster._id === 'deep_one') {
      // deep ones gain bonus for how far down their tile is on the board
      damage += cell.y()
    } else if (cell.monster._id === 'witch') {
      breaks += cell.monster.damage // witches always break wards equal to their damage
    } else if (cell.monster._id === 'wizard') {
      // wizard drains investigator magic and recovers health
      drains += Math.min(cell.monster.damage, investigator.magic.current) 
      if (drains > 0) {
        game.cells[cell._id].monster.health = game.cells[cell._id].monster.health + drains
        turn.alter(cell._id, {health: game.cells[cell._id].monster.health})
        turn.drain(drains)
      }
    } else if (cell.monster._id === 'ghast') {
      // ghasts deal extra damage per other monster on the board
      var packDamage = (monsterCells.length - 1) * cell.monster.damage
      damage += packDamage
    } else if (cell.monster._id === 'lloigor' && investigator.magic.current > 0) {
      // lloigor drain magic each turn instead of damage if investigator has magic
      drains += Math.min(cell.monster.damage, investigator.magic.current) 
      damage -= cell.monster.damage
    } else if (cell.monster._id === 'dark_young' && cell.isEvenRow()) {
      // dark young only deal 1 damage and terror on even rows
      damage -= cell.monster.damage-1 
      terror -= cell.monster.terror-1
    }
  }
  if (damage > 0) damage += exports.monsterDamageBonus(game.investigator.level)
  if (terror > 0) terror += exports.monsterTerrorBonus(game.investigator.level)

  var hallucinations = investigator.insanity('hallucinations') // hallucinations increase total monster terror
  if (hallucinations) {
    terror = Math.ceil(terror * (hallucinations.level * 1.5))
  }

  // calculate various investigator defenses
  var occult = investigator.skills.occult
  var insanity = investigator.insanity('horrors') // the horrors reduce insanity resistence
  if (insanity) occult = Math.min(occult - insanity.level, 0)

  damage = Math.max(damage - investigator.modifier('adventure'), 0)
  damage = Math.max(damage - investigator.wards.current, 0)
  damage = Math.max(damage - investigator.skills.adventure, 0)
  terror = Math.max(terror - occult, 0)
  terror = Math.max(terror - investigator.modifier('occult'), 0)
  drains = Math.min(drains, investigator.magic.current, 0)
  var breaks = exports.wardBreaks(game, cells, damage, breaks)

  turn.broke(breaks)
  turn.damage(Math.min(damage, investigator.health.current))
  turn.terror(Math.min(terror, investigator.sanity.current))
  turn.drain(drains)

  // update monsters in response to successful damage
  if (damage > 0 ) {
    for (var i = 0; i < monsterCells.length; i++) {
      var cell = monsterCells[i]
      if (cell.monster._id === 'nosferatu') {
        // nosferatu restore their health equal to their damage
        var life = cell.monster.health
        var maxLife = monsters['nosferatu'].health
        if (life < maxLife) {
          game.cells[cell._id].monster.health = Math.min(life+cell.monster.damge, maxLife)
          turn.alter(cell._id, {health: game.cells[cell._id].monster.health})
        }
      }
    }
  }

  // apply damage, terror, breaks, and drains
  investigator.modifyWards(-breaks)
  investigator.modifyHealth(-damage)
  investigator.modifySanity(-terror)
  investigator.gainMagic(-drains)

  // add insanity if lost all sanity
  if (investigator.sanity.current === 0) {
    var insanity = exports.madden(investigator) 
    turn.insane(insanity)
    game.stats.madness++
  }

  game.investigator = investigator
}