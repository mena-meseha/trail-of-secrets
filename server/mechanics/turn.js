var mongoose = require('mongoose');
var Game = mongoose.model('Game');
var Power = mongoose.model('Power');
var GOO = mongoose.model('GreatOldOne');
var advancement = require('./../mechanics/advancement')
var dealer = require('./../mechanics/dealer')
var matcher = require('./../mechanics/matcher')
var board = require('./../mechanics/board')
var mythos = require('./../mechanics/mythos')
var score = require('./../mechanics/score')
var powers = require('./../mechanics/powers')
var analytics = require('./../analytics')
var goo = require('./../models/great_old_one').Lookups

var Turn = function Turn() {
  this.board = []
  this.cells = null

  this.player = {
    health: 0,
    sanity: 0,
    wealth: 0,
    magic: 0,
    seals: 0,
    wards: 0,
    bonus: 0,
    exp: 0
  }
  this.attack = {
    damage: 0,
    terror: 0,
    breaks: 0,
    drains: 0
  }

  this.kills = []
  this.wounds = []
  this.plots = {
    solved: [],
    investigated: []
  }

  this.transforms = []
  this.alterations = []
  this.jumps = []
  this.tunnels = []

  this.level = {
    advancements: [],
    powers: null,
    experience: null,
    supplies: null
  }
  this.insanity = null
  this.item = null
  this.gameMenu = null
}
Turn.prototype.exp = function exp(delta) { this.player.exp += delta; }
Turn.prototype.bonus = function bonus(delta) { this.player.bonus += delta; }
Turn.prototype.seal = function seal(delta) { this.player.seals += delta; }
Turn.prototype.wealth = function wealth(delta) { this.player.wealth += delta; }
Turn.prototype.health = function health(delta) { this.player.health += delta; }
Turn.prototype.sanity = function sanity(delta) { this.player.sanity += delta; }
Turn.prototype.ward = function ward(current) { this.player.wards = current; }
Turn.prototype.magic = function magic(current) { this.player.magic = current; }
Turn.prototype.investigate = function investigate(index, clues) { 
  this.plots.investigated.push({_id: index, clues: clues})
}
Turn.prototype.solve = function solve(index) { 
  this.plots.solved.push(index)
}
Turn.prototype.kill = function kill(index, monsterId) { 
  this.kills.push({_id: index, monsterId: monsterId})
}
Turn.prototype.unkill = function unkill(index) { 
  for (var i = this.kills.length-1; i >= 0; i--) {
    if (this.kills[i]._id === index) {
      this.kills.splice(i, 1)
      break
    }
  }
}
Turn.prototype.wound = function wound(index, health) { 
  this.wounds.push({_id: index, health: health})
}
Turn.prototype.alterTile = function alterTile(index, tileId) {
  this.alter(index, {tile: tileId})
}
Turn.prototype.alter = function alter(index, alterations) {  
  alterations._id = index
  this.alterations.push(alterations)
}
Turn.prototype.transform = function transform(index, updates) { 
  updates._id = index
  this.transforms.push(updates)
}
Turn.prototype.damage = function damage(delta) { this.attack.damage += delta; }
Turn.prototype.terror = function terror(delta) { this.attack.terror += delta; }
Turn.prototype.broke = function broke(delta) { this.attack.breaks += delta; }
Turn.prototype.drain = function drain(delta) { this.attack.drains += delta; }
Turn.prototype.insane = function insane(insanity) { this.insanity = insanity; }
Turn.prototype.equip = function equip(item) { this.item = item.serialize(); }

Turn.prototype.jumpTile = function jumpTile(fromIndex, toIndex) {
  var tuple = [fromIndex, toIndex]
  this.jumps.push(tuple)
}
Turn.prototype.tunnel = function jumpTile(fromIndex, toIndex) {
  var tuple = [fromIndex, toIndex]
  this.tunnels.push(tuple)
}
Turn.prototype.reseatTile = function reseatTile(fromIndexOrNewTile, toIndex) {
  var tuple = [fromIndexOrNewTile, toIndex]
  this.board.push(tuple)
}
Turn.prototype.endGame = function endgame(outcome, score) {
  outcome = outcome === 'victory' ? 'victory' : 'defeat'
  this[outcome] = score
}
Turn.prototype.isEnded = function isEnded() {
  return this.vicatory || this.defeat ? true : false
}
Turn.prototype.experience = function experience(experience) { this.level.experience = experience; }
Turn.prototype.advance = function advance(advancement) { this.level.advancements.push(advancement); }
Turn.prototype.supply = function supply(supplies) { this.level.supplies = supplies; }
Turn.prototype.levelPowers = function levelPowers(powers) {
  this.level.advancements.push({powers: powers})
}
Turn.prototype.unlock = function unlock(difficulty) {
  this.level.unlock = difficulty
}
Turn.prototype.isMythosSkipped = function isMythosSkipped(game) {
  // mythos monsters move unless the plutonian drug was used
  return game.hasEffect('plutonian_drug')
}
Turn.prototype.serialize = function serialize() {
  var serialObject = { 
    player: {},
    attack: {}
  }
  if (this.board.length > 0) serialObject.board = this.board
  if (this.kills.length > 0) serialObject.kills = this.kills
  if (this.wounds.length > 0) serialObject.wounds = this.wounds
  if (this.cells) serialObject.cells = this.cells

  if (this.plots.investigated.length > 0 || this.plots.solved.length > 0) {
    serialObject.plots = {}
    if (this.plots.investigated.length > 0) serialObject.plots.investigated = this.plots.investigated
    if (this.plots.solved.length > 0) serialObject.plots.solved = this.plots.solved
  }

  if (this.transforms.length > 0) serialObject.transforms = this.transforms
  if (this.alterations.length > 0) serialObject.alterations = this.alterations
  if (this.jumps.length > 0) serialObject.jumps = this.jumps
  if (this.tunnels.length > 0) serialObject.tunnels = this.tunnels

  if (this.player.health !== 0) serialObject.player.health = this.player.health
  if (this.player.sanity !== 0) serialObject.player.sanity = this.player.sanity
  if (this.player.wealth !== 0) serialObject.player.wealth = this.player.wealth
  if (this.player.magic !== 0) serialObject.player.magic = this.player.magic
  if (this.player.wards !== 0) serialObject.player.wards = this.player.wards
  if (this.player.bonus !== 0) serialObject.player.bonus = this.player.bonus
  if (this.player.exp !== 0) serialObject.player.exp = this.player.exp
  if (this.player.seals !== 0) serialObject.player.seals = this.player.seals

  if (this.attack.damage !== 0) serialObject.attack.damage = this.attack.damage
  if (this.attack.terror !== 0) serialObject.attack.terror = this.attack.terror
  if (this.attack.breaks !== 0) serialObject.attack.breaks = this.attack.breaks
  if (this.attack.drains !== 0) serialObject.attack.drains = this.attack.drains
  if (this.insanity) serialObject.insanity = this.insanity
  if (this.item) serialObject.item = this.item

  if (this.level.advancements.length > 0 || this.level.powers) {
    serialObject.level = {}
    if (this.level.advancements.length > 0) serialObject.level.advancements = this.level.advancements
    if (this.level.supplies) serialObject.level.supplies = this.level.supplies
    if (this.level.experience) serialObject.level.experience = this.level.experience
    if (this.level.powers) serialObject.level.powers = this.level.powers      
    if (this.level.unlock) serialObject.level.unlock = this.level.unlock
  }

  if (this.gameMenu) serialObject.gameMenu = this.gameMenu

  if (this.victory) serialObject.victory = this.victory
  else if (this.defeat) serialObject.defeat = this.defeat

  return serialObject
}

function endGame(game, turn, cb) {
  // game is scored, and investigator adds to their cumulative score
  var investigator = game.investigator
  game.score = score.highScore(game)
  investigator.score += game.score
  game.investigator = investigator
  game.active = false

  // report final scores
  if (game.isInvestigatorDead()) {
    turn.endGame('defeat', game.score)
  } else if (game.isSealed()) {
    turn.endGame('victory', game.score)

    // update investigator difficulty (unlock more GOOs)
    var greatOldOne = goo[game.gooId]
    if (greatOldOne.difficulty > investigator.difficulty) {
      investigator.difficulty = greatOldOne.difficulty
      turn.unlock(greatOldOne.name)
    }

    // add new powers to select from after victory
    var powerMenu = Power.menu(game.investigator)
    turn.levelPowers(powerMenu)
    game.advancements.push({powers: powerMenu})

    // add menu of selectable goo for next game
    turn.gameMenu = GOO.menu(investigator)
  }

  // record player score
  game.investigator = investigator
  game.recordScore(cb)
}

exports.play = function(game, chain, cb) {
  // cannot play with pending advancements
  if (game.needsAdvancing()) return false 

  // record sampling of game state for analysis
  if (game.turn() % 10 === 0) analytics.sample(game)

  var initialExp = game.investigator.experience
  var turn = new Turn()

  matcher.match(game, turn, chain)

  // play mythos actions unless player victory
  if (!game.isSealed()) {
    mythos.update(game, turn)
    board.reseat(game, turn)

    if (!turn.isMythosSkipped(game)) {
      mythos.attack(game, turn)
      mythos.activity(game, turn)
    }
  }

  advancement.tally(game, turn)
  turn.exp( game.investigator.experience - initialExp )

  game.stats.moves++
  game.transpireEffects()

  // check for endgame conditions
  if (game.isInvestigatorDead() || game.isSealed()) {
    endGame(game, turn, function(err, score, userHighScore) {
      if (game.turn() % 10 !== 0) analytics.sample(game) // sample the game for endgame if not already sampled this turn
      cb(turn.serialize(), [score, userHighScore])
    })
  } else {
    cb(turn.serialize())
  }
}
exports.Turn = Turn