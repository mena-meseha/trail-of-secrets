var mongoose = require('mongoose');
var dealer = require('./dealer')
var matcher = require('./matcher')
var board = require('./board')
var Turn = require('./turn').Turn
var User = mongoose.model('User')

function canPurchase(investigator, item) {
  return investigator.wealth >= item.cost
}

function itemFromSupplies(game, itemId, usePatronage) {
  var investigator = game.investigator
  var supplies = game.supplies

  for (var i = 0; i < supplies.length; i++) {
    var item = supplies[i]
    if (item._id === itemId && !item.sold) {
      if (usePatronage || canPurchase(investigator, item)) {
        return item
      }
    }
  }
}

exports.applyEvent = function(game, item) {
  var investigator = game.investigator
  var turn = new Turn()
  game.investigator = investigator

  switch (item._id) {
    case 'space_mead':  
      // space mead redeals the entire board
      game.cells = dealer.newGameboard(game)
      turn.cells = game.serialize().cells

      break 
    case 'federal_raid':
      // turn all resources into weapons
      for (var i = 0; i < game.cells.length; i++) { 
        var cell = this.cells[i]
        if (cell.tileId === 'funds') {
          cell.tileId = 'firearm'
          turn.transform(cell._id, cell.serialize())
        } else if (cell.tileId === 'artifact') {
          cell.tileId = 'dynamite'
          turn.transform(cell._id, cell.serialize())
        }
      }
      break
    case 'hospitalization':  
      // hospitalization either restores 75% health or totally heals if investigator already has more
      var maxHealth = investigator.health.total * 0.75
      var newHealth = maxHealth > investigator.health.current ? maxHealth : investigator.health.total
      investigator.health.current = newHealth
      // hospitalization also takes 20 turns
      var hospitalDuration = 20
      game.stats.moves += hospitalDuration

      turn.health(investigator.health.current)
      turn.turns = hospitalDuration
      break 
    case 'hand_grenade':  
      // grenades damage all monsters on the board
      var cells = game.monsterCells()
      var grenadeDamage = 10
      var chain = []
      for (var i = cells.length-1; i >= 0; i--) {
        var cell = cells[i]
        var damageDealt = cell.attackMonster(cells, grenadeDamage, game.turn())
        turn.wound(cell._id, cell.monster.health)
        chain.push(cell._id)
      }
      matcher.clearMatches(game, chain)
      board.reseat(game, turn)
      break
    case 'shamans_brew':  
      var cells = game.monsterCells()
      for (var i = cells.length-1; i >= 0; i--) {
        var cell = cells[i]
        var terror = Math.ceil(cell.monster.terror / 2)
        var monster = game.cells[cell._id].monster
        monster.terror = terror
        game.cells[cell._id].monster = monster
        turn.transform(cell._id, {terror: terror})
      }
      break
    case 'powder_of_ibn_ghazi':  
      var cells = game.monsterCells()
      for (var i = cells.length-1; i >= 0; i--) {
        var cell = cells[i]
        game.cells[cell._id].monster.damage = Math.ceil(cell.monster.damage / 2)
        game.cells[cell._id].monster.defense = Math.ceil(cell.monster.defense / 2)
        turn.transform(cell._id, {damage: monster.damage, defense: monster.defense})
        console.log('POWDER '+Math.ceil(cell.monster.damage / 2)+', '+Math.ceil(cell.monster.defense / 2))
      }
      break
  }	
  game.investigator = investigator    
  return turn.serialize()
}

exports.applyRecovery = function(game, item) {
  var investigator = game.investigator
  var turn = new Turn()

  function tenPercentOrLessSanity() {
    var tenPercentSanity = Math.floor(investigator.sanity.total / 10) 
    return investigator.sanity.current > tenPercentSanity ? tenPercentSanity : 1
  }

  switch (item._id) {
    case 'black_lotus':
      // using black lotus restores magic and sets sanity to 10%
      investigator.sanity.current = tenPercentOrLessSanity()
      investigator.magic.current = investigator.magic.total

      turn.magic(investigator.magic.current)
      turn.sanity(investigator.sanity.current)
      break
    case 'first_aid_kit':
      var kitStrength = 20
      investigator.modifyHealth(kitStrength)
      turn.health(investigator.health.current)
      break   
    case 'elixir_of_life':
      investigator.health.current = investigator.health.total
      turn.health(investigator.health.current)
      break   
    case 'tourniquet':
      var tourniquetStrength = 10
      investigator.modifyHealth(tourniquetStrength)
      turn.health(investigator.health.current)
      break   
    case 'blood_transfusion':
      var transfusionStrength = 50
      investigator.modifyHealth(transfusionStrength)
      turn.health(investigator.health.current)
      break
    case 'panchrest':
      investigator.sanity.current = investigator.sanity.total
      investigator.health.current = investigator.health.total
      turn.health(investigator.health.current)
      turn.sanity(investigator.sanity.current)
      break
    case 'psychoanalysis':
      investigator.sanity.current = investigator.sanity.total
      investigator.wards.current = 0 // using psychoanalysis sets magic and wards to 0
      investigator.magic.current = 0
      turn.magic(investigator.magic.current)
      turn.ward(investigator.wards.current)
      turn.sanity(investigator.sanity.current)
      break      
    case 'whisky':
      var whiskyStrength = 10
      investigator.modifySanity(whiskyStrength)
      turn.sanity(investigator.sanity.current)
      break 
    case 'midnight_oil':
      var opiumStrength = 20
      investigator.modifySanity(opiumStrength)
      turn.sanity(investigator.sanity.current)
      break
    case 'toadstone':
      var toadstoneStrength = 5
      investigator.modifyWards(toadstoneStrength)
      turn.ward(investigator.wards.current)
      break   
    case 'adder_stone':
      var adderStoneStrength = 15
      investigator.modifyWards(adderStoneStrength)
      turn.ward(investigator.wards.current)
      break      
    case 'abramelin_oil':
      investigator.wards.current = investigator.wards.total
      turn.ward(investigator.wards.current)
      break
    case 'reanimation_serum':
      // using the reanimation serum restores health and sets sanity to 10%
      investigator.sanity.current = tenPercentOrLessSanity()
      investigator.health.current = investigator.health.total

      turn.health(investigator.health.current)
      turn.sanity(investigator.sanity.current)
      break
    case 'four_species':
      // using the four species gains 1 health, 1 sanity, 1 ward, 1 magic for each monster on the board
      var cnt = game.monsterCells().length
      if (cnt > 0) {
        if (investigator.health.current < investigator.health.total) {
          investigator.modifyHealth(cnt)
          turn.health(investigator.health.current)
        }
        if (investigator.sanity.current < investigator.sanity.total) {
          investigator.modifySanity(cnt)
          turn.sanity(investigator.sanity.current)
        }
        if (investigator.wards.current < investigator.wards.total) {
          investigator.modifyWards(cnt)
          turn.ward(investigator.wards.current)
        }
        if (investigator.sanity.current < investigator.sanity.total) {
          investigator.gainMagic(cnt)
          turn.magic(investigator.magic.current)
        }
      }
      break
    case 'yithian_mind_trip':
      // taking the trip gains 33% of exp to next lelvel, to a max of -1 point away from next level
      var nextLevelExp = investigator.nextLevelExperience()
      var exp = investigator.level > 1 ? (nextLevelExp-levelExperience(investigator.level-1) ) : nextLevelExp
      var thirdOfExp = Math.floor(exp / 3) 

      if (investigator.experience + thirdOfExp > nextLevelExp) thirdOfExp = nextLevelExp - investigator.experience - 1
      investigator.experience += thirdOfExp

      turn.exp(investigator.sanity.thirdOfExp)
      break
  }	
  game.investigator = investigator
  return turn.serialize()
}

exports.applyEquipment = function(game, item) {
  var investigator = game.investigator
  var turn = new Turn()

  item.equip(investigator)
  game.investigator = investigator

  turn.equip(item)
  return turn.serialize()
}

exports.supply = function(game, itemId, usePatronage, cb) {
  var item = itemFromSupplies(game, itemId, usePatronage)
  var results = {}

  console.log('supply item '+item+ " (usePatronage? "+usePatronage+")")
  if (item) {
    switch (item.type) {
      case 'recovery':
        results = exports.applyRecovery(game, item)
        break
      case 'event':
        results = exports.applyEvent(game, item)
        break
      case 'effect':
        if (itemId === 'plutonian_drug') {
          game.addEffect('binding', true)
        } else {
          game.addEffect(itemId, true)
        }
        break
      case 'accessory':
      case 'attire':
      case 'weapon':
        results = exports.applyEquipment(game, item)
        break
    }

    // purchase item based on game cash currency or patronage premium currency
    if (usePatronage && game.userId) {
      // buy with patronage
      User.findOne({'_id': game.userId}, function(err, user) {
        if (user) {
          game.stats.patronage += item.sell(user, true)
          user.save(function(err) {
            cb(null, results)
          })
        } else {
          cb('could not find user id '+game.userId+': '+err)
        }
      })
    } else if (usePatronage) {
      cb('cannot use patronage without registered user for game.userId ('+game.userId+')')
    } else {
      // buy with cash
      var investigator = game.investigator
      results.cost = item.sell(investigator)
      game.stats.spent += results.cost
      game.investigator = investigator

      cb(null, results)
    }
  } else {
    cb('item '+itemId+' not found') 
  }
}