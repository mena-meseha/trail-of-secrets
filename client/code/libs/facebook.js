var APP_ID = '469824099725037'
var User = require('/player').User

// set global accessor
$.facebook = {data: false, status: null}

function loadFacebookUser(user) {
  if (user) {
    $.user = new User(user)
  }
}

function authorize(payload) {
  $.facebook.data = payload // cache user data

  // // console.log('authorizing')
  // ss.rpc('user.authorize', payload.signedRequest, function(err, user) {
  //   // if user missing basic info then try to retrieve it from their social graph
  //   var needsUserData = user && !user.name

  //   if (needsUserData) {
  //     // console.log("needs UserData facebook data")
  //     FB.api('/me', function(response) {
  //       ss.rpc('user.update', response)
  //     });
  //   }

  //   // console.log('authorized '+err+' '+JSON.stringify(user))

  //   // store facebook id reference (if present)
  //   loadFacebookUser(user)

  //   // if a game already loaded while waiting for authorization, reload with the correct game for user
  //   var needsGameReload = user && user.gameId && $.tos && (user.gameId !== $.tos.gameId)
  //   if (needsGameReload) {
  //     // console.log("NEED TO RELOAD GAME!!!! user.gameId="+user.gameId+', $.tos.gameId='+$.tos.gameId)
  //     if ($.tos.gameboard) {
  //       // console.log('REstarting game')
  //       $.tos.gameboard.kill(function() {
  //         // $.tos.startGame()
  //       })
  //     } else {
  //       // console.log('starting game')
  //       // $.tos.startGame()
  //     }
  //   }
  // })
}

function loginToFacebook(e) {
  e.preventDefault()
  $.facebook.login()
}

function updateLoginStatus(response) {
  console.log('fb updateLoginStatus called '+JSON.stringify(response))

  var login = $('#login')
  login.unbind('click', loginToFacebook)

  function setLoginLabel(text) {
    login.children('span').html(text)
  }

  $.facebook.status = response.status
  if (response.status === 'connected') {
    authorize(response.authResponse) 
    login.parent().hide('')
  } else if (response.status === 'not_authorized') {
    login.bind('click', loginToFacebook)
    setLoginLabel('Signup')
  } else {
    login.bind('click', loginToFacebook)
    setLoginLabel('Login')
  }
};

$.facebook.currency = function(cb) {
  // get currency settings from user cache or fb graph
  if ($.user.currency) {
    cb($.user.currency)
  } else {
    FB.api('/me?fields=currency', function(response) {
      // console.log("currency "+JSON.stringify(response))
      $.user.currency = response ? response.currency : null
      cb($.user.currency)
    });
  }
}

$.facebook.permissions = function(cb) {
  if ($.user.facebookPermissions) {
    cb($.user.facebookPermissions)
  } else {
    FB.api('/me/permissions', function (response) {
      // console.log('/me/permissions='+JSON.stringify(response))
      $.user.facebookPermissions = response.data[0]
      cb($.user.facebookPermissions)
    });
  } 
}

$.facebook.score = function(points, cb) {
  $.facebook.permissions(function(permissions) {
    if (permissions.publish_actions === 1) {
      FB.api('/me/scores', 'post', {score: points}, function(response) {
        if (typeof cb === 'function') cb()
      })
    } else {
      // TODO : show facebook request permissions dialog 
      if (typeof cb === 'function') cb()
    }
  })
}
$.facebook.scores = function(cb) {
  FB.api('/'+APP_ID+'/scores', function(response) {
    cb(response)
  })
}

$.facebook.login = function(cb) {
  // todo: cache login credentials and return immediately if already available
  if ($.facebook.data) {
    cb($.facebook.data)
  } else {
    try {
      FB.login(function(response) {
        // console.log("FB.login : "+JSON.stringify(response))

        if (response.authResponse) {
          // connected, so authorize user
          authorize(response.authResponse)
        } else {
          // cancelled
        }
        cb($.facebook.data)
      });
    } catch (e) {
      // if FB SDK cannot be loaded then just ignore
      cb()
    }
  }
}

$.facebook.buy = function(itemId, cb) {
  var obj = {
    method: 'pay',
    action: 'buy_item',
    order_info: {'itemId': itemId},
    dev_purchase_params: {'oscif': true}
  };
  
  if ($.facebook.isCanvasIFrame()) {
    FB.ui(obj, function(data) {

      switch (itemId) {
        case 'unexpected_inheritance':
          user.patronage += 10
          break;
        case 'mysterious_benefactor':
          user.patronage += 50
          break;
        case 'masonic_fellowship':
          user.patronage += 100
          break;
        case 'wilmarth_foundation_endowment':
          user.patronage += 1000
          break;
      }

    });  
  } else {
    $.facebook.goToCanvas()
  }
}

$.facebook.goToCanvas = function() {
  window.location = 'https://apps.facebook.com/trailofsecrets/game/';
}
$.facebook.isCanvasIFrame = function() {
  return window.location !== window.parent.location
}

var loadSDKTimeout = null
var fbConnectUrl = "//connect.facebook.net/en_US/all.js"
var sdkLoadInterval = 15000

function loadFacebookSDK() {
  // prevent JQ from appending cache busting string to the end of the FeatureLoader URL 
  var cacheSetting = jQuery.ajaxSettings.cache; 
  jQuery.ajaxSettings.cache = true;

  jQuery.getScript(fbConnectUrl)
  .success(function() {
    // clear interval after successful load
    if (loadSDKTimeout) clearTimeout(loadSDKTimeout)
  })
  .fail(function() {
  })
  .done(function() {
  })

  // restore cache setting
  jQuery.ajaxSettings.cache = cacheSetting;
}

(function(d){
  // Load the SDK asynchronously and keep retrying until library loads
  loadSDKTimeout = setInterval( loadFacebookSDK, sdkLoadInterval)

  loadFacebookSDK()
}(document));

window.fbAsyncInit = function() {
  FB.init({
    appId: APP_ID, 
    status: true, 
    cookie: true, 
    oauth: true,
    xfbml: true,
    channelURL: window.location.origin+'/channel'
  })
  
  $('#login').click(loginToFacebook)  
  FB.Event.subscribe('auth.statusChange', updateLoginStatus)
}