var User = function(userData) {
  this.patronage = userData ? userData.patronage : 0
  this.facebookId = userData ? userData.facebookId : null
  this.facebookPermissions = {}
  this.currency = null
}
User.prototype.isAuthorized = function isAuthorized() {
  return this.facebookId ? true : false
}
User.prototype.modifyPatronage = function modifyPatronage(delta) {
  this.patronage = Math.max(this.patronage+delta, 0)
}

exports.User = User

$.user = new User()