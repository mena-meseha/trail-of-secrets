var Monster = require('/monster').Monster;

var Cell = function(cell, x, y, index, tile) {
  this.cell = cell
  this.x = x
  this.y = y
  this.index = index
  this.tile = tile

  if (tile) tile.tile.appendTo(this.cell)

  return this
}
Cell.prototype.clear = function clear() {
  this.tile = null
  this.cell.find(':not(.puff)').remove() // remove html contents, except puff animations which will remove themselves
}
Cell.prototype.hasMonster = function hasMonster(monsterId) {
  return this.tile.model.monster && (!monsterId || this.tile.model.monster._id === monsterId)
}
Cell.prototype.setMonster = function setMonster(monster) {
  if (this.tile.monster) this.tile.tile.removeClass(this.tile.monster.model._id)
  this.tile.tile.addClass(monster._id)

  this.tile.monster = new Monster(this.tile.tile, monster)
  this.tile.model.monster = monster
  this.tile.tile.data('type', 'monster')
  this.tile.tile.attr('data-type', 'monster')

  this.tile.model.tileId = 'monster'
  this.tile.model.tileType = 'combat'

}

Cell.prototype.isEvenRow = function () {
  return this.y % 2 !== 0
}
Cell.prototype.isDeactivated = function isDeactivated() {
  return this.tile.tile.hasClass('deactivated')
}
Cell.prototype.isSelected = function isSelected() {
  return this.tile.tile.hasClass('selected')
}
Cell.prototype.deactivate = function deactivate() {
  if (this.tile) this.tile.tile.addClass('deactivated')
}
Cell.prototype.select = function select(cells, classes) {
  classes = classes || (this.tile.monster ? 'selected target' : 'selected')
  this.tile.tile.addClass(classes)
}
Cell.prototype.deselect = function deselect() {
  this.tile.tile.removeClass('selected target muted')
}
Cell.prototype.match = function match() {
  if (this.tile.monster) {
    this.tile.tile.addClass('animated short infinite flash')
    if (this.tile.monster && this.tile.monster.model._id === 'fire_vampire') {
      this.tile.tile.addClass('bounce flash')
    }
  } else if (this.tile.plot) {
    this.tile.tile.addClass('animated short infinite flash')
  } else {
    if (this.tile.model.tileId === 'bandages' || this.tile.model.tileId === 'morphine') {
      $.tos.hud.health.score(this.tile.tile)
    } else if (this.tile.model.tileId === 'tincture' || this.tile.model.tileId === 'laudanum') {
      $.tos.hud.sanity.score(this.tile.tile)
    } else if (this.tile.model.tileType === 'magic') {
      if ($.tos.hud.wards.current < $.tos.hud.wards.total) {
        $.tos.hud.wards.score(this.tile.tile)
      } else {
        $.tos.hud.magic.score(this.tile.tile)
      }
    } else if (this.tile.model.tileType === 'resource') {
      $.tos.hud.wealth.score(this.tile.tile)
    } else if (this.tile.model.tileType === 'seal') {
      $.tos.hud.seals.score(this.tile.tile)
    }
    this.tile.tile.addClass('animated spin-out')
  }
}
Cell.prototype.spinOut = function spinOut() {
  this.tile.tile.addClass('animated spin-out')
}
Cell.prototype.fadeOut = function fadeOut() {
  this.tile.tile.addClass('fade-out-down')
}
Cell.prototype.puff = function puff(cb) {
  var temp = this.tile.tile.clone()
  temp.find('.progress, .badge.stat').remove()
  temp.css('position', 'absolute').css('top', 0).css('left', 0).css('z-index', 9999)
  temp.appendTo(this.cell).addClass('animated puff long')

  setTimeout(function() {
    temp.remove()
  }, 1500)

  if (typeof cb === 'function') cb()
}
exports.Cell = Cell