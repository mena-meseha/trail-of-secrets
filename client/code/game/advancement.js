$.tos.advanced = function(results) {
  if (!results) return

  if (results.health) $.tos.hud.health.increase(results.health)
  if (results.sanity) $.tos.hud.sanity.increase(results.sanity)
  if (results.wards) $.tos.hud.wards.increase(results.wards)
  if (results.magic) $.tos.hud.magic.increase(results.magic)
  if (results.adventure) $.tos.hud.addAdventure(results.adventure)
  if (results.conflict) $.tos.hud.addConflict(results.conflict)
  if (results.occult) $.tos.hud.addOccult(results.occult)
  if (results.research) $.tos.hud.addResearch(results.research)
  if (results.hunting) $.tos.hud.addHunting(results.hunting)
  if (results.healing) $.tos.hud.addHealing(results.healing)
  if (results.funding) $.tos.hud.addFunding(results.funding)
  if (results.augury) $.tos.hud.addAugury(results.augury)
  if (results.arcana) $.tos.hud.addArcana(results.arcana)
}

$.tos.advancing = function(json, cb) {
  // keep showing modal until all advances are taken

  var showAdvancement = function() {
    if (json.length > 0) {
      var menu = json[0]
      json.splice(0,1)

      // advancements can be a skill/stat menu, or else in the case of victory, a powers menu
      if (menu.powers) {
        $.tos.dialogs.power(menu.powers, cb)
      } else {
        $.tos.dialogs.level(menu, json.length > 0 ? showAdvancement : cb)
      }
    }
  }
  showAdvancement()
}