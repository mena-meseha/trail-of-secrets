function usingCdn() {
  var origin = window.location.origin
  return origin && origin.indexOf('trailofsecrets.com') ? true : false
}

function usingiDevice() {
  return navigator.userAgent.match(/(ipad|iphone|ipod)/i)
}

$(document).ready(function() {
  var soundtrack = null
  var soundCookieName = 'tos.music'

  function saveSoundSettings(playSounds) {
    $.cookie(soundCookieName, playSounds);
  }
  function playSounds() {
    return $.cookie(soundCookieName)+'' === 'true' || $.cookie(soundCookieName) === null ? true : false
  }
  function showPause() {
    $('#audio').children('i').removeClass('icon-music').addClass('icon-pause')
  }
  function showPlay() {
    
  }

  function loopSound(sound) {
    sound.play({
      onfinish: function() {
        loopSound(sound);
      },
      onplay: function() {
      }
    });
  }  

  function loadSoundtrack() {
    var autoplay = playSounds()
    if (!autoplay) showPause()

    var host = usingCdn() ? 'https://s3.amazonaws.com/trailofsecrets/static' : ''

    soundtrack = soundManager.createSound({
      id:'bgMp3',
      url: host+'/sounds/dagon_rises.mp3',
      autoLoad: true,
      autoPlay: autoplay,
      onload: function() {
        if (autoplay) loopSound(this)
      },
    });
  }

  function loadSoundtrackOnIDevice() {
    loadSoundtrack()
    $(document).unbind('mousedown', loadSoundtrackOnIDevice)
  }

  soundManager.setup({
    url: '/flash/',
    flashVersion: 8,
    debugMode: true,
    useFlashBlock: false,
    onready: function() {
      if (usingiDevice()) {
        $(document).on("mousedown", $(document), loadSoundtrackOnIDevice)
      } else {
        loadSoundtrack()
      }
    },
    ontimeout: function() {
    },    
  });

  $('#audio').click(function() {
    if (!soundtrack) return

    if ($(this).children('i').hasClass('icon-music')) {
      $(this).children('i').removeClass('icon-music').addClass('icon-pause')
      soundtrack.pause()
      saveSoundSettings(false)
    } else {
      $(this).children('i').removeClass('icon-pause').addClass('icon-music')
      soundtrack.play()
      saveSoundSettings(true)
    }
  })

});