var Monster = require('/monster').Monster;

function updatePlots(results) {
  if (!results.plots) return

  if (results.plots.solved) {
    for (var i = 0; i < results.plots.solved.length; i++) {
      var index = results.plots.solved[i]
      var cell = $.tos.gameboard.cellFromIndex(index)

      cell.puff(function() {
        cell.tile.solvePlot()
      })
    }
  }
  if (results.plots.investigated) {
    for (var i = 0; i < results.plots.investigated.length; i++) {
      var plot = results.plots.investigated[i]
      var cell = $.tos.gameboard.cellFromIndex(plot._id)
      cell.tile.plot.setClues(plot.clues)
    }   
  }
}

$.tos.updateMonster = function(update) {
  var index = parseInt(update._id)
  var cell = $.tos.gameboard.cellFromIndex(index)
  var tile = cell.tile.tile

  if (update.monster) {
    // transform entire into new monster 
    cell.puff(function(){
      cell.setMonster(update.monster)
    })
  } else {
    // update current tile monster
    var monster = cell.tile.monster
    if (update.health) monster.setHealth(update.health)
    if (update.defense) monster.setDefense(update.defense)
    if (update.damage) monster.setDamage(update.damage)
    if (update.terror) monster.setTerror(update.terror)
  }
}

$.tos.playTurn = function(results, cb) {
  var deferMatchUpdates = $.Deferred();
  var deferBoardUpdates = $.Deferred();
  var deferMonsterAttacks = $.Deferred();
  var deferMonsterActivity = $.Deferred();
  var deferAdvancement = $.Deferred();

  if (results.player || results.plots || results.transforms) {
    var stackedMeterLag = 400 // give top stacked UI element time for the bottom element to move

    // update player HUD stats
    if (results.player.health) {
    var delay = results.player.sanity ? stackedMeterLag : 0
      setTimeout( function() { $.tos.hud.health.modify(results.player.health) }, delay)
    }
    if (results.player.sanity) $.tos.hud.sanity.modify(results.player.sanity)
    if (results.player.wards) $.tos.hud.wards.modify(results.player.wards)
    if (results.player.wards && results.player.magic) {
      setTimeout( function() { $.tos.hud.magic.modify(results.player.magic) }, stackedMeterLag)
    } else if (results.player.magic) {
      $.tos.hud.magic.modify(results.player.magic)
    }

    if (results.player.wealth) {
      $.tos.hud.wealth.modify(results.player.wealth)
      $.tos.hud.model.wealth += results.player.wealth
    }
    if (results.player.seals) {
      $.tos.hud.seals.modify(results.player.seals) 
      $.tos.hud.seals.bounce()
    }
    if (results.player.exp) $.tos.hud.exp.modify(results.player.exp)
    if (results.player.bonus) {
      var msg = '+'+results.player.bonus+' bonus match'+(results.player.bonus === 1 ? '' : 'es')
      $.tos.gameboard.flash(msg)
    }

    // update gameboard plots
    updatePlots(results)

    // update any monsters or tiles affected by the match
    if (results.wounds) {
      for (var i = 0; i < results.wounds.length; i++) {
        var update = results.wounds[i]
        $.tos.updateMonster(update)
      }
    }
    if (results.transforms) {
      for (var i = 0; i < results.transforms.length; i++) {
        var update = results.transforms[i]
        $.tos.updateMonster(update)
      }
    }

    var matchDelay = 150
    setTimeout( function() { 
      deferMatchUpdates.resolve()
    }, matchDelay)
  } else {
    deferMatchUpdates.resolve()
  }

  // update tile positions
  $.when(deferMatchUpdates).done(function() {
    if (results.board) {
      $.tos.reseatBoard(results.board, deferBoardUpdates.resolve)
    } else {
      deferBoardUpdates.resolve()
    }
  })

  // monsters attack
  $.when(deferBoardUpdates).done(function() {
    if (results.victory) {
      $.tos.advancing(results.level.advancements, function() {
        cb(null, 'victory')
      })
      return;
    } else {
      $.tos.monsterMods()
      $.tos.monsterAttack(results, function() {
        deferMonsterAttacks.resolve()
      })
    }
  })

  // update monster special ability results
  $.when(deferMonsterAttacks).done(function() {
    if (results.alterations || results.jumps || results.tunnels) {
      $.tos.monsterActivity(results, function() {
        deferMonsterActivity.resolve()
      })
    } else {
      deferMonsterActivity.resolve()
    }
  })

  // show advancements (unless game is lost)
  $.when(deferMonsterActivity).done(function() {
    if (results.defeat) {
      deferAdvancement.resolve() 
    } else if (results.level && results.level.advancements && results.level.advancements.length > 0) {
      $.tos.hud.addLevels(results.level.advancements.length)
      var exp = {
        current: results.level.experience.current, 
        total: results.level.experience.next
      }
      $.tos.hud.exp._load(exp)
      $.tos.supplies = results.level.supplies

      $.tos.advancing(results.level.advancements, function() {
        deferAdvancement.resolve() 
      })
    } else {
      deferAdvancement.resolve() 
    }
  })  

  $.when(deferAdvancement).done(function() {
    if (typeof cb === 'function') cb(null, results.defeat ? 'defeat' : 'continue')
  })
}
