var Monster = require('/monster').Monster;
var Plot = require('/plot').Plot;

var Tile = function(index, gameboard, json, cssClass) {
  this.model = json
  this.tile = $("<a data-index='"+index+"' data-type='"+this.model.tileId+"' class='"+this.model.tileType+"'></a>")
  if (cssClass) this.tile.addClass(cssClass)

  this.tile.bind('mousedown', gameboard.startSelection)
  this.tile.bind('mouseover', gameboard.moveSelection)
  this.tile.bind('mouseup', gameboard.endSelection)
  this.tile.touchit({
    onTouchMove: gameboard.touchSelection,
    onTouchEnd: gameboard.endSelection 
  });  

  if (json.monster) {
    this.monster = new Monster(this.tile, json.monster)
    this.tile.addClass(json.monster._id)
  } else if (json.plot) {
    this.plot = new Plot(this.tile, json.plot)
    this.tile.addClass(this.model.tileId)
  } else {
    this.tile.addClass(this.model.tileId)
  }

  return this
}
Tile.prototype.isUpgradable = function isUpgradable() {
  var tid = this.model.tileId
  return tid === 'tincture' || tid === 'rite' || tid === 'bandages' || tid === 'funds' || tid === 'firearm'
}
Tile.prototype.solvePlot = function solvePlot() {
  this.tile.find('.progress').remove()
  this.changeType('seal', 'seal')
}
Tile.prototype.changeType = function changeType(type, tid) {
  this.model.tileId = tid
  this.model.tileType = type

  this.tile.attr('data-type', this.model.tileId).data('type', this.model.tileId)
  this.tile.attr('class', type+' '+tid)

  if (type !== 'plot') {
    this.plot = null
    this.model.plot = null
  }
  if (type !== 'monster') {
    this.monster = null
    this.model.monster = null
  }

  this.tile.removeClass('animated short long infinite flash pulse')
}

exports.Tile = Tile