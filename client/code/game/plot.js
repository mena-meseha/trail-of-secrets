var Plot = function(tile, json) {
  this.tile = tile
  this.model = json
  this.model.clues = this.model.clues || 0

  var width = parseInt((this.model.clues / this.model.level) * 100)
  var label = "<label><span class='clues'>"+(json.clues || 0)+"</span>/<span class='level'>"+json.level+"</span></label>"
  var bar = $("<div class='progress progress-danger'><div class='bar'>"+label+"</div></div>")

  this.tile.append(bar)
  this._updateLabel()
  this._updateBar()

  return this
}
Plot.prototype._updateBar = function _updateBar() {
  var percentage = parseInt(100 - (this.model.clues / this.model.level) * 100)
  var bar = this.tile.find('.bar')
  bar.css('width', percentage+'%')
}
Plot.prototype._updateLabel = function _updateLabel() {
  var label = this.tile.find('.clues')
  label.html(this.model.level - this.model.clues)
}
Plot.prototype.modifyLevel = function modifyLevel(delta) {
  this.model.clues = Math.max(0, this.model.clues + delta)
  this._updateLabel()
  this._updateBar()
}
Plot.prototype.setClues = function setClues(value) {
  this.model.clues = value
  this._updateLabel()
  this._updateBar()
}
Plot.prototype.setLevel = function setLevel(value) {
  this.model.clues = value
  this._updateLabel()
  this._updateBar()
}
Plot.prototype.isSolved = function isSolved() {
  return this.model.clues >= this.model.level
}

exports.Plot = Plot