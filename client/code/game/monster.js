var Monster = function(tile, json) {
  this.tile = tile
  this.model = json

  var label = "<label><span class='health'>"+this.model.health+"</span>/<span class='life'>"+this.model.life+"</span></label>"
  var bar = $("<div class='progress progress-danger'><div class='bar'>"+label+"</div></div>")

  var damage = $("<span class='badge badge-important stat pull-left damage'>"+this.model.damage+"</span>")
  var terror = $("<span class='badge badge-info stat pull-left terror'>"+this.model.terror+"</span>")
  var defense = $("<span class='badge badge-warning stat pull-right defense'>"+this.model.defense+"</span>")

  this.tile.empty()
  this.tile.append(damage)
  this.tile.append(terror)
  this.tile.append(defense)
  this.tile.append(bar)

  this._updateLabel()
  this._updateBar()

  return this
}
Monster.prototype._updateBar = function _updateBar() {
  var percentage = parseInt((this.model.health / this.model.life) * 100)
  var bar = this.tile.find('.bar')
  bar.css('width', percentage+'%')
}
Monster.prototype._updateLabel = function _updateLabel() {
  var label = this.tile.find('.health')
  label.html(this.model.health)
  label = this.tile.find('.damage')
  label.html(this.model.damage)
  label = this.tile.find('.defense')
  label.html(this.model.defense)
  label = this.tile.find('.terror')
  label.html(this.model.terror)
}
Monster.prototype._update = function _update() {
  this._updateLabel()
  this._updateBar()
}
Monster.prototype.modifyHealth = function modifyHealth(delta) {
  this.model.health = Math.max(0, this.model.health + delta)
}
Monster.prototype.setHealth = function setHealth(value) {
  this.model.health = value
  this._update()
}
Monster.prototype.setDefense = function setHealth(value) {
  this.model.defense = value
  this._update()
}
Monster.prototype.setDamage = function setHealth(value) {
  this.model.damage = value
  this._update()
}
Monster.prototype.setTerror = function setHealth(value) {
  this.model.terror = value
  this._update()
}
Monster.prototype.isDead = function isDead() {
  return this.model.health < 1
}

exports.Monster = Monster