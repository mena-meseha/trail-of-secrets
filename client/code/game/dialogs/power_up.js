var popup;

function empower(powerId) {
  return function(e) {
    e.preventDefault()
    $.tos.empower(powerId, function(results) { 
      popup.modal('hide')
    })
    return false
  }
}

function powerRow(power) {
  var row = $("<tr><td><strong>"+power.name+"</strong><br /><em>"+power.description+"</em></td></tr>")
  var btn = $("<a class='btn btn-primary'>Empower</a>")

  btn.click(empower(power._id))
  var cell = $('<td></td>').append(btn)
  row.append(cell)
  return row
}

exports.Menu = function Menu(powers, cb) {
  var popup = $("<div id='power-up' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>")
  var header = $("<div class='modal-header'><h2>You sealed the evil!</h2></div>")
  var body = $("<div class='modal-body'><p>Prevailing against the darkness, you can now choose a new Mythos power.</p></div>")
  var table = $("<table class='table'></table>")

  // cleanup and invoke callback (if applicable)
  popup.on('hidden', function(){
    popup.remove()
    popup = null
    if (typeof cb === 'function') cb()
  })  

  for (var i = 0; i < powers.length; i++) {
    table.append( powerRow(powers[i]) )
  }

  popup.append(header).append(body).append(table).appendTo('body')

  if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  popup.modal('show')
}