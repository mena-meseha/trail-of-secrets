var popup;

function advance(advanceId) {
  return function(e) {
    e.preventDefault()
    popup.modal('hide')
    $.tos.advance(advanceId, $.tos.input.idle)
    return false
  }
}

function advancementRow(advancement) {
  var row = $("<tr><th>"+advancement.name+"</th></tr>")
  row.append("<td><span class='label label-success'>+"+advancement.delta+" "+advancement._id+"</span></td>")

  var btn = $("<a class='btn btn-primary'>Advance</a>")
  btn.click( advance(advancement._id) )
  var cell = $('<td></td>').append(btn)
  row.append(cell)

  return row
}

exports.Menu = function Menu(menu, cb) {
  if (!(menu && menu.level)) {
    cb()
    return
  }

  // initialize popup in DOM if it does not already exist
  popup = $("<div id='advancement' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>")
  var header = $("<div class='modal-header'><h3>Advanced to Level "+menu.level+"!</h3></div>")
  var body = $("<div class='modal-body'><p>Choose an advancement to improve your skills.</p></div>")
  var table = $("<table class='table'></table>")

  popup.append(header).append(body).append(table).appendTo('body')

  var table = popup.find('table.table')
  for (var i = 0; i < menu.advancements.length; i++) {
    table.append( advancementRow(menu.advancements[i]) )
  }  

  // cleanup and invoke callback (if applicable)
  popup.on('hidden', function(){
    popup.remove()
    popup = null
    if (typeof cb === 'function') cb()
  })

  if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  popup.modal('show')
}
