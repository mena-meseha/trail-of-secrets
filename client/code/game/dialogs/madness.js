var popup;

function cleanup(cb) {
  return function() {
    popup.remove()
    popup = null
    if (typeof cb === 'function') cb()
  }
}

exports.Menu = function Menu(results, cb) {
  console.log(results)
  popup = $("<div id='maddened' class='modal hide' tabindex='-1' role='dialog' aria-hidden='true'>")
  var header = $("<div class='modal-header'><h3>You were driven INSANE</h3></div>")
  var body = $("<div class='modal-body'>The horror, the horror; you are now suffering from a case of <big>"+results.insanity.name+"</big></div>")
  var footer = $("<div class='modal-footer'><button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button></div>")

  // update UI
  $.tos.hud.sanity.reset()
  $.tos.insanity.addInsanity(results.insanity)

  // cleanup and invoke callback (if applicable)
  popup.on('hidden', function(){
    setTimeout(cleanup(cb), 100)
  })

  popup.append(header).append(body).append(footer).appendTo('body')
  popup.modal('show')
}