var popup;

function selectGame(e) {
  e.preventDefault()
  var gooId = $(this).attr('href').replace('#', '')

  popup.find('a').unbind('click')
  $.tos.newGame(gooId)

  return false
}

exports.Menu = function Menu(menu) {
  popup = $("<div id='game-menu' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>")
  var header = $("<div class='modal-header'><h2>Game Select</h2></div>")
  var body = $("<div class='modal-body'></div>")  
  body.append("<p>Choose a Great Old One to challenge for the next game.</p>")

  var table = $("<table class='grid'></table>")
  table.append("<tr><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td></tr>")
  table.append("<tr><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td></tr>")
  table.append("<tr><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td><td><a class='blank'>?</a></td></tr>")

  function selectGame(e) {
    e.preventDefault()
    var gooId = $(this).attr('href').replace('#', '')

    popup.find('a').unbind('click')
    $.tos.newGame(gooId)

    return false
  }

  var cells = table.find('td')
  for (var i = 0; i < menu.length; i++) {
    var goo = menu[i]
    var btn = $("<a href='#"+goo._id+"'><span class='goo'></span><br />"+goo.name+"<br /><em>Level "+goo.difficulty+"</em></a>")
    btn.click(selectGame)

    $(cells[i]).html(btn)
  }
  body.append(table)

  popup.append(header).append(body).appendTo('body')

  if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  popup.modal('show')  
}