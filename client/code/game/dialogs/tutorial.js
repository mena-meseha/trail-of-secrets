var popup;
var tutorialCookieName = 'tos.tutorial'
var matchDemoAnimation, 
    fightDemoAnimation,
    plotDemoAnimation

function howToMatchSlide() {
  var item = $("<div class='item active'></div>")
  var board = "<table id='game' cellspacing='0' cellpadding='0'><tr><td><a class='resource funds'></a></td><td><a class='resource funds'></a></td><td><a class='resource bandages'></a></td><td><a class='resource funds'></a></td></tr><tr><td><a class='combat firearm'></a></td><td><a class='resource rite'></a></td><td><a class='resource funds'></a></td><td><a class='resource funds'></a></td></tr></table>"
  var caption = "<div stlye='marign-top:20px' class='carousel-caption'><h4>1. How to Match</h4><p>Drag over three or more tiles of the same type and release to match a chain. Retrace your trail to cancel a match.</p></div>"
  item.append(board).append(caption)

  var frame = 0
  var matchDemoAnimation = setInterval( function(){
    var cells = item.find('table a')
    switch (frame) {
      case 1:
        $(cells[0]).addClass('selected')
        break
      case 2:
        $(cells[1]).addClass('selected')
        break
      case 3:
        $(cells[6]).addClass('selected')
        break
      case 4:
        $(cells[7]).addClass('selected')
        break
      case 5:
        $(cells[3]).addClass('selected')
        break
      case 8:
        $(cells[3]).removeClass('selected')
        break
      case 9:
        $(cells[7]).removeClass('selected')
        break
      case 11:
        $(cells[6]).removeClass('selected').addClass('hidden')
        $(cells[1]).removeClass('selected').addClass('hidden')
        $(cells[0]).removeClass('selected').addClass('hidden')
        break
      case 14:
        $(cells[6]).removeClass('hidden')
        $(cells[1]).removeClass('hidden')
        $(cells[0]).removeClass('hidden')
        break
    }
    frame++
    if (frame > 14) frame = 0
  }, 150)

  return item
}
function howtoFightSlide() {
  var item = $("<div class='item' cellspacing='0' cellpadding='0'></div>")
  var board = "<table id='game'><tr><td><a class='combat firearm'></a></td><td><a class='combat cultist'></a></td><td><a class='investigation clue'></a></td><td><a class='resource rite'></a></td></tr><tr><td><a class='recovery bandages'></a></td><td><a class='combat firearm'></a></td><td><a class='combat high_priest'></a></td><td><a class='combat firearm'></a></td></tr></table>"
  var caption = "<div class='carousel-caption'><h4>2. How to Fight</h4><p>Match monsters with weapons to damage them. Monsters that remain on the board will attack you every turn; press and hold a tile to view any special abilities.</p></div>"
  item.append(board).append(caption)

  var frame = 0
  var fightDemoAnimation = setInterval( function(){
    var cells = item.find('table a')
    switch (frame) {
      case 1:
        $(cells[0]).addClass('selected')
        break
      case 2:
        $(cells[1]).addClass('selected target')
        break
      case 3:
        $(cells[5]).addClass('selected')
        break
      case 4:
        $(cells[6]).addClass('selected target')
        break
      case 5:
        $(cells[7]).addClass('selected')
        break
      case 8:
        $(cells[7]).removeClass('selected').addClass('hidden')
        $(cells[6]).removeClass('selected target')
        $(cells[5]).removeClass('selected').addClass('hidden')
        $(cells[1]).removeClass('selected target').addClass('hidden')
        $(cells[0]).removeClass('selected').addClass('hidden')
        break
      case 11:
        $(cells[7]).removeClass('hidden')
        $(cells[1]).removeClass('hidden')
        $(cells[5]).removeClass('hidden')
        $(cells[0]).removeClass('hidden')
        break
    }
    frame++
    if (frame > 11) frame = 0
  }, 150)

  return item
}
function howToInvestigateSlide() {
  var item = $("<div class='item' cellspacing='0' cellpadding='0'></div>")
  var board = "<table id='game'><tr><td><a class='investigation clue'></a></td><td><a class='seal seal'></a></td><td><a class='combat firearm'></a></td><td><a class='resource grimoire'></a></td></tr><tr><td><a class='investigation clue'></a></td><td><a class='investigation plot'></a></td><td><a class='seal seal'></a></td><td><a class='resource funds'></a></td></tr></table>"
  var caption = "<div class='carousel-caption'><h4>3. How to Win</h4><p>Match clues with plots to solve them and turn them into seals, and match seals to win the game.</p></div>"
  item.append(board).append(caption)

  var frame = 0
  var plotDemoAnimation = setInterval( function(){
    var cells = item.find('table a')
    switch (frame) {
      case 1:
        $(cells[0]).addClass('selected')
        break
      case 2:
        $(cells[4]).addClass('selected')
        break
      case 3:
        $(cells[5]).addClass('selected')
        break
      case 5:
        $(cells[0]).removeClass('selected').addClass('hidden')
        $(cells[4]).removeClass('selected').addClass('hidden')
        $(cells[5]).removeClass('selected plot').addClass('seal')
        break
      case 7:
        $(cells[6]).addClass('selected')
        break
      case 8:
        $(cells[5]).addClass('selected')
        break
      case 9:
        $(cells[1]).addClass('selected')
        break
      case 10:
        $(cells[6]).addClass('hidden')
        $(cells[5]).addClass('hidden')
        $(cells[1]).addClass('hidden')
        break
      case 13:
        $(cells[1]).removeClass('selected')
        $(cells[5]).removeClass('selected seal').addClass('plot')
        $(cells[6]).removeClass('selected')
        $(cells[0]).removeClass('hidden')
        $(cells[1]).removeClass('hidden')
        $(cells[4]).removeClass('hidden')
        $(cells[5]).removeClass('hidden')
        $(cells[6]).removeClass('hidden')
        break
    }
    frame++
    if (frame > 13) frame = 0
  }, 150)

  return item
}

function cleanup() {
  if (matchDemoAnimation) clearInterval(matchDemoAnimation) 
  if (fightDemoAnimation) clearInterval(fightDemoAnimation) 
  if (plotDemoAnimation) clearInterval(plotDemoAnimation) 

  popup.remove()
  popup = null
}

function complete() {
  popup.modal('hide')  
  $.cookie(tutorialCookieName, true)
}

var currentSlide = 0
var maxSlide = 2
function showNext(nextButton, skipButton) {
  nextButton.html('Next')
  skipButton.removeClass('disabled')
}
function showFinish(nextButton, skipButton) {
  nextButton.html('Finish')
  skipButton.addClass('disabled')
}
function previousSlide(carousel, nextButton, skipButton) {
  return function(e) {
    carousel.carousel('prev') 
    currentSlide = currentSlide === 0 ? maxSlide : currentSlide-1
    if (currentSlide == maxSlide) {
      showFinish(nextButton, skipButton)
    } else {
      showNext(nextButton, skipButton)
    }
  }
}
function nextSlide(carousel, nextButton, skipButton) {
  return function(e) {
    if (currentSlide === maxSlide) {
      complete()
      return
    }

    carousel.carousel('next') 
    currentSlide = currentSlide > maxSlide-1 ? 0 : currentSlide+1
    if (currentSlide === maxSlide) {
      showFinish(nextButton, skipButton)
    } else {
      showNext(nextButton, skipButton)
    }
  }
}

exports.Menu = function Menu() {
  popup = $("<div id='tutorial' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true'>")
  var header = $("<div class='modal-header'><h3>Quickstart Guide</h3></div>")
  var body = $("<div class='modal-body'></big></div>")
  var carousel = $("<div class='carousel-inner'></div>")
  carousel.append(howToMatchSlide())
  carousel.append(howtoFightSlide())
  carousel.append(howToInvestigateSlide())
  body.append(carousel)

  var footer = $("<div class='modal-footer'></div>")
  var prev = $("<button class='btn' aria-hidden='true'>Previous</button>")
  var next = $("<button class='btn' aria-hidden='true'>Next</button>")
  var skip = $("<button class='btn' aria-hidden='true'>Skip</button>")
  currentSlide = 0

  popup.on('hidden', cleanup)  
  prev.click( previousSlide(carousel, next, skip) )
  next.click( nextSlide(carousel, next, skip) )
  skip.click( complete )

  footer.append(prev).append(next).append(skip)
  popup.append(header).append(body).append(footer).appendTo('body')
  popup.modal('show')  
  carousel.carousel({interval: false})
}