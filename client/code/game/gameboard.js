var Tile = require('/tile').Tile;
var Cell = require('/cell').Cell;
var Chain = require('/chain').Chain;

function numericSortDescending(a, b){ return (b-a) }

$.fn.gameboard = function(cells, effects) {
  var self = this

  this.inputMode = 'selection' // selection | picker
  this.cells = []
  this.width = this.find('tr').first().children('td').length
  this.height = this.find('tr').length
  this.animationSpeed = 800
  this.effects = effects || []

  function isInTileBounds(tile, x, y) {
    return !(x <= tile.offset().left || 
          x >= tile.offset().left + tile.outerWidth() ||
             y <= tile.offset().top  || 
             y >= tile.offset().top + tile.outerHeight() )
  }  

  function cellFromTouch(x, y) {
    var table = self.find('td')

    for (var i = 0; i < table.length; i++) {
      var td = $(table[i])
      if (isInTileBounds(td, x, y)) {
        var tile = td.children('a')
        return self.cellFromIndex(tileIndex(tile))
      }
    }
  }
  this.cellFromIndex = function(index) {
    return self.cells[self.yFromIndex(index)][self.xFromIndex(index)]
  }
  function tileIndex(tile) {
    return parseInt($(tile).attr('data-index'))
  }

  // assuming a square grid...
  this.xFromIndex = function(index) { return index % self.width }
  this.yFromIndex = function(index) { return Math.floor(index / self.height) }
  this.isIndexAdjacent = function(index, matchIndex) {
    if (!self.chain || self.chain.isEmpty()) return true

    matchIndex = matchIndex || self.chain.currentIndex()
    var x = self.xFromIndex(matchIndex)
    var y = self.yFromIndex(matchIndex)
    var cell = self.cellFromIndex(index)

    for (var i = y-1; i <= y + 1; i++) {
      for (var j = x -1; j <= x + 1; j++) {
        if (cell.x === j && cell.y === i) return true
      }
    }

    return false
  }
  this.cellsByType = function(type) {
    var cells = []
    for (y = 0; y < self.cells.length; y++) { 
      for (x = 0; x < self.cells[y].length; x++) { 
        var cell = self.cells[y][x]
        if (type === null || cell.tile.model.tileType === type) cells.push(cell)
      }
    }
    return cells
  }
  this.monsterCells = function() {
    var cells = []
    for (y = 0; y < self.cells.length; y++) { 
      for (x = 0; x < self.cells[y].length; x++) { 
        var cell = self.cells[y][x]
        if (cell.tile.monster) cells.push(cell)
      }
    }
    return cells
  }

  // Gameboard has two types of input - dragging selection, or click-based picker
  this.inputMode = 'selector'
  this.pickerMode = function(tileCount, cb) {
    self.setInputMode('picker')
    self.tileCount = tileCount
    self.pickerCallback = cb
  }
  this.hasPickedAll = function() {
    return self.chain.cells.length >= self.tileCount
  }
  this.selectorMode = function() {
    self.setInputMode('selector')
    self.tileCount = null
    self.pickerCallback = null
  }
  this.setInputMode = function(mode) {
    this.inputMode = mode || 'selector'
  }
  this.canSelectAndPick = function(cell) {
    return !cell.isDeactivated() &&
           !$.tos.input.isResolving()
  }
  this.selectCell = function(cell) {
    if (!self.canSelectAndPick(cell)) return

    self.chain = self.chain || new Chain(self)
    if (self.chain.select(cell)) {
      $.tos.input.active()
      $.tos.hud.setTitle()
    }
  }  
  this.pickCell = function(cell) {
    if (!self.canSelectAndPick(cell)) return

    self.chain = self.chain || new Chain(self)
    if (!(self.chain.isEmpty() || self.isIndexAdjacent(cell.index) )) {
      return false
    } else {
      self.chain.add(cell)
      self.deactivateCells()

      if (self.hasPickedAll()) {
        self.pickerCallback(self.chain.cells, function() {
          // return to default selection mode
          self.selectorMode() 
          $.tos.input.idle()
          self.deselect()
        })
      }
    }
  }

  this.tilesMatch = function(t1, t2) {
    return t1 && t2 && t1.model.tileType === t2.model.tileType
  }
  this.deactivateTiles = function(tile) {
    for (y = 0; y < self.cells.length; y++) { 
      for (x = 0; x < self.cells[y].length; x++) { 
        var cell = self.cells[y][x]
        if (!self.tilesMatch( tile, cell.tile )) {
          cell.deactivate()
        }
      }
    }
  }
  this.deactivateCells = function(cell) {
    for (y = 0; y < self.cells.length; y++) { 
      for (x = 0; x < self.cells[y].length; x++) {
      var cell = self.cells[y][x] 
        if (!self.isIndexAdjacent(cell.index)) {
          cell.deactivate()
        }
      }
    }
  }
  this.reactivateTiles = function() {
    self.find('a.deactivated').removeClass('deactivated')
  }
  this.deselect = function() {
    self.find('.selected').removeClass('selected target muted')
    self.chain.cancelMonsterInfo().clear()
    self.chain = null
    self.reactivateTiles();
  }    
  this.stopAnimations = function() {
    self.find('.animated').removeClass('animated short infinite flash spin-out bounce')  
  }

  this.startSelection = function() {
    if ($.tos.input.isWaiting()) return // ignore if UI disabled

    var cell = self.cellFromIndex(tileIndex(this))
    if (!cell) return // safeguard

    if (self.inputMode === 'selector') {
      if (cell) self.selectCell(cell)
    } else if (self.inputMode === 'picker') {
      if (cell) self.pickCell(cell)
    }

  }

  this.moveSelection = function() {
    if (!$.tos.input.isActive()) return

    var cell = self.cellFromIndex(tileIndex(this))
    if (cell) self.selectCell(cell)
  } 

  this.touchSelection = function(x, y) {
    if (!$.tos.input.isActive()) return

    var cell = cellFromTouch(x, y)
    if (cell) self.selectCell(cell)
  }  

  this.endSelection = function() {
    if (!$.tos.input.isActive()) return

    if (self.chain.isMatchComplete()) {
      self.playMatch( self.chain.cells )
    } else {
      $.tos.input.idle()
    }
    self.deselect()
  }

  this.playMatch = function(cells) {
    $.tos.playMatch(cells, $.tos.matchResults);
  }

  this.addTile = function(index, json) {
    var cell = self.cellFromIndex(index)
    cell.clear()
    cell.tile = new Tile(index, self, json, 'offscreen')

    cell.tile.tile.appendTo(cell.cell)
    cell.tile.tile.addClass('animated drop-in').removeClass('offscreen')

    setTimeout( function() {
      cell.tile.tile.removeClass('animated drop-in')
    }, 800);
  }

  this.swapTiles = function(fromIndex, toIndex, animate) {
    var fromCell = self.cellFromIndex(fromIndex)
    var toCell = self.cellFromIndex(toIndex)
    var fromJson = fromCell.tile.model
    var toJson = toCell.tile.model

    var deferFrom = $.Deferred();
    var deferTo = $.Deferred();

    $.when(deferFrom, deferTo).done(function() {
      toCell.tile =  new Tile(toCell.index, self, fromJson)
      toCell.tile.tile.appendTo(toCell.cell)

      fromCell.tile =  new Tile(fromCell.index, self, toJson)
      fromCell.tile.tile.appendTo(fromCell.cell)
    })

    if (animate === false) {
      deferFrom.resolve()
      deferTo.resolve()
    } else {
      self.animateMoveTile(toCell, fromCell, deferTo.resolve, true)
      self.animateMoveTile(fromCell, toCell, deferFrom.resolve, true)
    }
  }
  this.moveTile = function(fromIndex, toIndex) {
    var fromCell = self.cellFromIndex(fromIndex)
    var toCell = self.cellFromIndex(toIndex)
    var json = fromCell.tile.model
    var deltaY = toCell.y - fromCell.y

    // empty target cell and append moving tile, animating the transition with a drop-in class
    toCell.clear()
    toCell.tile =  new Tile(toCell.index, self, json)
    toCell.tile.tile.addClass("animated short drop-in-"+deltaY).appendTo(toCell.cell)
  }
  this.animateMoveTile = function(cell, fromCell, cb, doNotEmpty) {
    var tile = fromCell.tile.tile
    var oldOffset = tile.offset();

    var temp = tile.clone().hide().appendTo('#game')
    temp.css('position', 'absolute')
        .css('left', oldOffset.left)
        .css('top', oldOffset.top)
        .css('zIndex', 1000)

    tile.remove()
    var newOffset = cell.cell.offset()

    if (!doNotEmpty) cell.cell.empty()

    var opts = {}
    if (cell.y !== fromCell.y) opts.top = newOffset.top
    if (cell.x !== fromCell.x) opts.left = newOffset.left

    temp.show().animate(opts, self.animationSpeed, function() {
      temp.remove()
      if (typeof(cb) === "function") cb()
    })
  }

  function dropIn(tile, duration) {
    setTimeout( function() {
      tile.addClass('animated short drop-in').removeClass('offscreen')
    }, duration);
  }

  this.flash = function(message, delay) {
    var bonusMessage = $("<div class='animated fade-out-up longer message'>"+message+"</div>")
    $('#game').prepend(bonusMessage)
    delay = delay || 1750
    setTimeout( function() { bonusMessage.remove(); }, delay)
  }

  this.addEffect = function(name, value) {
    self.effects.push([name, value])
  }
  this.hasEffect = function(name, value) {
    if (self.effects.length > 0) {
      for (i = 0; i < this.effects.length; i++) {
        var effect = this.effects[i]
        var matchingEffect = effect[0] === name
        var matchAnyValue = value === undefined || effect[1] === undefined
        if (matchingEffect && (matchAnyValue || value === effect[1])) return true
      }
    }
    return false
  }
  this.clearEffects = function() {
    if (self.effects.length > 0) {
      self.effects = []
      self.find('.binding').removeClass('binding')
    }
  }
  this.kill = function(cb) {
    self.find('td a').addClass('animated fade-out-down shortest')
    if (typeof cb === 'function') {
      setTimeout(cb, 250);
    }
  }

  var table = this.find('td')
  this.loadCells = function(cells) {
    self.find('a').remove()
    for (y = 0; y < this.height; y++) {
      this.cells.push([])
      for (x = 0; x < this.width; x++) {
        var index = (y * this.width) + x
        var td = $(table[index])
        td.empty()
        var tile = new Tile(index, self, cells[index], 'offscreen')
        var cell = new Cell(td, x, y, index, tile)

        var delay = (((this.height -y -1) * 50) * 1.5) + (x * 25)
        dropIn(tile.tile, delay)

        this.cells[y].push( cell )
      }
    }
  }

  // initiaize board cells
  this.loadCells(cells)

  // display any active game effects
  for (i = 0; i < this.effects.length; i++) {
    var effect = this.effects[i]
    if (effect[0] === 'binding') {
      var index = effect[1]
      if (index === true) {
        var cells = self.monsterCells()
        for (c = 0; c < cells.length; c++) {
          cells[c].tile.tile.addClass('binding')
        }
      } else {
        var cell = self.cellFromIndex(index)
        if (cell.tile.monster) cell.tile.tile.addClass('binding')
      }
    }
  }

  setTimeout( function() { self.find('.animated').removeClass('animated short drop-in') }, 1500 ) // remove animation classes 
  setTimeout( $.tos.input.idle, 2000);  // wait 2 seconds before enabling UI

  return self
}