$.tos.useItem = function(results, cb) {
  if (!results) {
    cb();
    return
  }

  var deferItemUpdates = $.Deferred();
  var deferActivity = $.Deferred();
  var deferBoardUpdates = $.Deferred();

  var stackedMeterLag = 400 // give top stacked UI element time for the bottom element to move

  if (results.player) {
    if (results.player.health) {
      var delay = results.player.sanity ? stackedMeterLag : 0
      setTimeout( function() { $.tos.hud.health.set(results.player.health) }, delay)
    }
    if (results.player.sanity) $.tos.hud.sanity.set(results.player.sanity)
    if (results.player.wards) {
      var delay = results.player.magic ? stackedMeterLag : 0
      setTimeout( function() { $.tos.hud.wards.set(results.player.wards) }, delay)
    }
    if (results.player.magic) $.tos.hud.magic.set(results.player.magic)
    if (results.player.wealth) {
      $.tos.hud.wealth.set(results.player.wealth)
      $.tos.hud.model.wealth = results.player.wealth
    }
    if (results.player.seals) $.tos.hud.seals.set(results.player.seals)
    if (results.player.exp) $.tos.hud.exp.set(results.player.exp)
  }

  if (results.turns) {
    $.tos.hud.incrementTurn(results.turns)
    $.tos.hud.turn.addClass('animated long flash')
    setTimeout( function() { $.tos.hud.turn.removeClass('animated long flash') }, 1500)
  }

  if (results.item) {
    $.tos.hud.setEquipment(results.item.type, results.item)
    $.tos.hud.setSkills()
  }

  if (results.effect) {
    if (results.effect === 'plutonian_drug') {
      var cells = $.tos.gameboard.monsterCells()
      for (var i = 0; i < cells.length; i++) {
        cells[i].tile.tile.addClass('binding')
        $.tos.gameboard.addEffect('binding', cells[i]._id)
      }
    }
  }

  if (results.wounds) {
    for (var i = 0; i < results.wounds.length; i++) {
      var update = results.wounds[i]
      var cell = $.tos.gameboard.cellFromIndex(update._id)
      cell.tile.tile.addClass('animated short infinite flash')
      $.tos.updateMonster(update)
    }
    setTimeout( function() { 
      $('#gameboard .flash').removeClass('animated short infinite flash') 
      deferActivity.resolve()
    }, 1200)
  } else if (results.transforms) {
    for (var i = 0; i < results.transforms.length; i++) {
      var update = results.transforms[i]
      $.tos.updateMonster(update)
    }
    setTimeout( deferActivity.resolve, 1200)
  } else {
    deferActivity.resolve()
  }

  $.when(deferActivity).done(function() {
    if (results.cells) {
      $.tos.loadCells(results.cells)
    } else if (results.board) {
      $.tos.reseatBoard(results.board, deferBoardUpdates.resolve)
    } else {
      deferBoardUpdates.resolve()
    }
  })


  $.when(deferBoardUpdates).done(function() {
    var msg = 'You equipped '+results.item.name+'!'
    if (results.item.type === 'recovery' || results.item.type === 'event' || results.item.type === 'effect') {
      msg = 'You used '+results.item.name+'!'
    }
    $.tos.gameboard.flash(msg, 2500)
  })

  cb()
}
