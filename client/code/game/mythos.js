var Tile = require('/tile').Tile;

function checkInsanity(results, cb) {
  if (results.insanity && !(results.defeat || results.victory)) {
    $.tos.dialogs.madness(results, cb)
  } else if (typeof cb === 'function') {
    cb()
  }
}

function updateTile(update) {
  var index = parseInt(update._id)
  var cell = $.tos.gameboard.cellFromIndex(index)
  if (update.tile) {
    var type = update.tile === 'funds' ? 'resource' : (update.tile === 'rite' ? 'magic' : update.tile)
    cell.tile.changeType(type, update.tile)
  }
}

exports.monsterActive = function monsterActive(cell) {
  cell.tile.tile.addClass('animated short bounce')
}

function monsterTunnel(tunnel) {
  var fromCell = $.tos.gameboard.cellFromIndex(tunnel[0])
  var toCell = $.tos.gameboard.cellFromIndex(tunnel[1])
  fromCell.tile.tile.addClass('animated short shrink-away')
  toCell.tile.tile.addClass('animated short shrink-away')
  var fromJson = fromCell.tile.model
  var toJson = toCell.tile.model
  var oldFrom = fromCell.tile.tile.hide()
  var oldTo = toCell.tile.tile.hide()

  tunnelDelay = 500
  setTimeout( function() {
    toCell.tile =  new Tile(toCell.index, $.tos.gameboard, fromJson, 'animated short grow-in')
    toCell.tile.tile.appendTo(toCell.cell)

    fromCell.tile =  new Tile(fromCell.index, $.tos.gameboard, toJson, 'animated short grow-in')
    fromCell.tile.tile.appendTo(fromCell.cell)

    oldTo.remove()
    oldFrom.remove()
  
    setTimeout( function() {
    $('#gameboard .grow-in').removeClass('short grow-in')
    }, tunnelDelay)
  }, tunnelDelay); 
}

function monsterJumpOut(jump) {
  var fromCell = $.tos.gameboard.cellFromIndex(jump[0])
  var toCell = $.tos.gameboard.cellFromIndex(jump[1])
  fromCell.tile.tile.addClass('animated short bounce-out-up')
  toCell.tile.tile.addClass('animated short bounce-out-up')
}

function monsterJumpIn(jump) {
  var fromCell = $.tos.gameboard.cellFromIndex(jump[0])
  var toCell = $.tos.gameboard.cellFromIndex(jump[1])
  var fromJson = fromCell.tile.model
  var toJson = toCell.tile.model

  var oldFrom = fromCell.tile.tile.hide()
  var oldTo = toCell.tile.tile.hide()

  toCell.tile =  new Tile(toCell.index, $.tos.gameboard, fromJson, 'animated short bounce-in-down')
  toCell.tile.tile.appendTo(toCell.cell)

  fromCell.tile =  new Tile(fromCell.index, $.tos.gameboard, toJson, 'animated short bounce-in-down')
  fromCell.tile.tile.appendTo(fromCell.cell)

  oldTo.remove()
  oldFrom.remove()
}

function monsterTada(attack, tile, delay) {
  setTimeout(function() { 
    if (attack.breaks) {
      $.tos.hud.wards.score(tile, 'break')
      if (attack.damage) {
        setTimeout( function() {
          var origin = $.tos.hud.wards.bar
          $.tos.hud.health.score(origin, 'damage')
        }, 750)
      }
    } else if (attack.damage) {
      $.tos.hud.health.score(tile, 'damage')
    }

    if (attack.sanity) $.tos.hud.sanity.score(tile, 'madness')
    if (attack.drains) $.tos.hud.magic.score(tile, 'magic')
    tile.find('.progress, .stat').toggle()
    tile.addClass('animated tada')
  }, delay);
}

$.tos.monsterActivity = function(results, cb) {
  if (results.alterations) {
    for (var i = 0; i < results.alterations.length; i++) {
      var update = results.alterations[i]
      if (update.tile) {
        updateTile(update)
      } else {
        $.tos.updateMonster(update)
      }
    }
  }

  var jumpDelay = 0
  if (results.jumps) {
    for (var i = 0; i < results.jumps.length; i++) {
      monsterJumpOut(results.jumps[i])
    }

    var jumpInDelay = 250
    // swap tiles and aniamte monsters returning
    setTimeout( function() {
      for (var i = 0; i < results.jumps.length; i++) {
        var jump = results.jumps[i]
        monsterJumpIn(jump)
      }
    }, jumpInDelay);

    jumpDelay = 1000
    setTimeout( function() {
      $('#gameboard .bounce-in-down').removeClass('animated short bounce-in-down')
    }, jumpDelay); 
  }

  var tunnelDelay = 0
  if (results.tunnels) {
    tunnelDelay = 500

    for (var i = 0; i < results.tunnels.length; i++) {
      monsterTunnel(results.tunnels[i])
    }
  }

  // activate
  var activated = false
  var alterationDelay = 0
  var cells = $.tos.gameboard.monsterCells()
  var hadKills = results.kills && results.kills.length > 0
  var hadFundsAltered = false
  for (var i = 0; i < (turn.alterations || []).length; i++) {
    if (turn.alterations.tile && turn.alterations.tile === 'funds') {
      hadFundsAltered = true
      alterationDelay = 750
      break
    }
  }
  var hasRitesAltered = false
  for (var i = 0; i < (turn.alterations || []).length; i++) {
    if (turn.alterations.tile && turn.alterations.tile === 'rite') {
      hasRitesAltered = true
      alterationDelay = 750
      break
    }
  }

  for (var i = 0; i < cells.length; i++) {
    var cell = cells[i]
    var monsterId = cell.tile.monster._id
    if (hadKills && monsterId === 'ghoul') {
      monsterActive(cell)
      activated = true
    } else if (monsterId === 'formless_spawn') {
      monsterActive(cell)
      activated = true
    } else if (hadFundsAltered && monsterId === 'mi_go') {
      monsterActive(cell)
      activated = true
    } else if (hasRitesAltered && monsterId === 'rat_thing') {
      monsterActive(cell)
      activated = true
    }
  }
  // cleaup activation animations
  if (activated) {
    var activationDelay = 500
    setTimeout( function() {
      $('#gameboard .bounce').removeClass('animated short bounce')
    }, activationDelay); 
  }

  var delay = 300 + Math.max(Math.max(jumpDelay, tunnelDelay), alterationDelay)
  setTimeout(function() { 
    if (typeof cb === 'function') cb()
  }, delay );  
}


$.tos.monsterMods = function(cb) {
  var cells = $.tos.gameboard.monsterCells()

  function toggleMod(condition, gameCell, damage, terror) {
    if (condition) {
      if (damage) gameCell.cell.find('.damage').addClass('mod')
      if (terror) gameCell.cell.find('.terror').addClass('mod')
    } else {
      if (damage) gameCell.cell.find('.damage').removeClass('mod')
      if (terror) gameCell.cell.find('.terror').removeClass('mod')
    }
  }

  var hasHighPriests = false
  for (var i = 0; i < cells.length; i++) {
    var monster = cells[i].tile.monster
    if (monster.model._id === 'high_priest') hasHighPriests = true
  }
  for (var i = 0; i < cells.length; i++) {
    var monster = cells[i].tile.monster
    // high priests make cultists stronger
    if (monster.model._id === 'cultist') {
      toggleMod( hasHighPriests, cells[i], true, false )
    } else if (monster.model._id === 'werewolf') {
      var isFullMoon = $.tos.hud.turnCount % 5 === 0
      toggleMod( isFullMoon, cells[i], true, true )
    } else if (monster.model._id === 'ghast') {
      toggleMod( cells.length > 1, cells[i], true, false )
    } else if (monster.model._id === 'deep_one') {
      toggleMod( cells[i].y > 0, cells[i], true, false )
    } else if (monster.model._id === 'dark_young') {
      toggleMod( cells[i].isEvenRow(), cells[i], true, true )
    }
  }
}

$.tos.monsterAttack = function(results, cb) {
  var cells = $.tos.gameboard.monsterCells()
  if (cells.length === 0) {
    if (typeof cb === 'function') cb()
    return
  }

  var maxAnimationDelay = 200
  var hasBindings = $.tos.gameboard.hasEffect('binding')
  for (var i = 0; i < cells.length; i++) {
    var tile = cells[i].tile
    if (hasBindings && $.tos.gameboard.hasEffect('binding', cells[i].index)) {
      continue
    }

    var delay = Math.floor(Math.random() * maxAnimationDelay)
    if (!tile.monster.isDead()) monsterTada(results.attack, tile.tile, delay)
  }

  var hudDelay = 1000
  setTimeout(function() { 
    if (results.attack.damage) {
      $.tos.hud.health.flash(-results.attack.damage)
      $.tos.hud.health.modify(-results.attack.damage)
    }
    if (results.attack.terror) {
      $.tos.hud.sanity.flash(-results.attack.terror)
      $.tos.hud.sanity.modify(-results.attack.terror)
    }
    if (results.attack.breaks) {
      $.tos.hud.wards.flash(-results.attack.breaks)  
      $.tos.hud.wards.modify(-results.attack.breaks)  
    }
    if (results.attack.drains) {
      $.tos.hud.magic.flash(-results.attack.drains)  
      $.tos.hud.magic.modify(-results.attack.drains)  
    }
  }, hudDelay)

  function cleanup() {
    $('.animated.tada').removeClass('animated tada')
    $('a .progress, a .stat').show()
    $.tos.removeScores()
  }

  setTimeout(function() { 
    cleanup()
    checkInsanity(results, cb)
  }, 1200 );  
}