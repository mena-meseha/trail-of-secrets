function endPowerSelect() {
  $.tos.setInput('idle')
  $.tos.gameboard.power = null
  $.tos.gameboard.deselect()
}

function deactivateNonMonsterCells() {
  var cells = $.tos.gameboard.cellsByType(null)
  for (var i = 0; i < cells.length; i++) {
    if (!cells[i].tile.monster) cells[i].deactivate()
  }
}

$.fn.powers = function(powers) {
  var self = this
  this.cells = self.find('li')

  function powerLink(power) {
    var link = $('<a></a>')
    link.append('<span class="power '+power._id+'" />')
    link.append('<big>'+power.name+'</big><br />')
    link.append('<small>Lvl. '+power.level+'</small> |')
    link.append(' <span>'+power.magic+' magic</span>')
    link.append('<small class="desc">'+power.description+'</small>')
    return link
  }

  function notEnoughPower(power, link) {
    if (power.magic > $.tos.hud.model.magic.current) {
      var label = link.find('span').last()
      var icon = $('.icon-tos.magic')
      label.addClass('animated short infinite flash')
      icon.addClass('animated short infinite flash')
      setTimeout( function() { 
        label.removeClass('animated short infinite flash'); 
        icon.removeClass('animated short infinite flash'); 
      }, 1500)
      return true
    }
    return false
  }

  this.addPower = function(power, cell) {
    var link = powerLink(power)

    var invoke = function(e) {
      if ($.tos.input.isWaiting()) return // ignore if UI is disabled
      if (notEnoughPower(power, link)) return // can't play power without enough magic

      var selectable = power._id === 'legerdemain' || 
                       power._id === 'the_great_work' || 
                       power._id === 'body_warping_of_gorgoroth' || 
                       power._id === 'solomons_wisdom' || 
                       power._id === 'supernal_binding'
      if (selectable) link.addClass('selected')
      self.invokePower(power._id)
    }

    link.click(invoke)
    link.appendTo(cell)
  }
  
  for (var i = 0; i < powers.length; i++) {
  	var cell = $(self.cells[i])
  	cell.html('')
    self.addPower(powers[i], cell)
  }

  this.idle = function() {
    self.find('a.selected').removeClass('selected')
  }
  this.invokePower = function(powerId) {
    if ($.tos.input.isWaiting()) return false // ignore if UI disabled

    if ($.tos.input.isPower()) { // clicking power twice cancels power mode
      self.idle()
      $.tos.gameboard.selectorMode()
      $.tos.gameboard.deselect()
      $.tos.input.idle()
      return false
    }

    var deferResolving = $.Deferred();
    var deferRPC = $.Deferred();
    $.tos.input.power()
    switch (powerId) {
      case 'as_above_so_below':
        function swap(fromIndex, toIndex, delay) {
          setTimeout(function(){          
            $.tos.gameboard.swapTiles(fromIndex, toIndex)
          },delay)
        }

        var swapSpeed = 400
        var animationSpeed = $.tos.gameboard.animationSpeed
        $.tos.gameboard.animationSpeed = swapSpeed
        delay = 0
        for (var y = 4; y < 7; y++) {
          for (var x = 0; x < 7; x++) {
            var index = $.tos.gameboard.cells[y][x].index
            var mirrorIndex = $.tos.gameboard.cells[6-y][x].index
            $.tos.gameboard.swapTiles(index, mirrorIndex)
          }
        }
        setTimeout(deferResolving.resolve, delay*swapSpeed + (swapSpeed/2))
        $.tos.playPower(powerId, null, deferRPC.resolve)

        $.when(deferRPC, deferResolving).done(function(results) {
          $.tos.input.idle()
        })
        break;
      case 'solomons_wisdom':
        $.tos.gameboard.pickerMode(1, function(cells, cb) {
          var index = cells.pop()
          var cell = $.tos.gameboard.cellFromIndex(index)

          function swap(a, b, delay) {
            setTimeout(function(){          
              $.tos.gameboard.swapTiles(a.index, b.index)
            },delay)
          }
          
          var swapSpeed = 250
          var animationSpeed = $.tos.gameboard.animationSpeed
          $.tos.gameboard.animationSpeed = swapSpeed
          delay = 0
          for (var i = 0; i <7; i++) {
            var from = $.tos.gameboard.cells[cell.y][i]
            var to = $.tos.gameboard.cells[i][cell.x]

            if (from.index === to.index) continue

            swap(from, to, delay*swapSpeed+50)
            delay = i
          }
          setTimeout(deferResolving.resolve, delay*swapSpeed + (swapSpeed/2))
          $.tos.playPower(powerId, [index], deferRPC.resolve)

          $.when(deferRPC, deferResolving).done(function(results) {
            $.tos.gameboard.animationSpeed = animationSpeed
            cb()
            $.tos.input.idle()
            self.idle()
          })
        })
        break;
      case 'legerdemain':
        $.tos.gameboard.pickerMode(2, function(cells, cb) {
          var isValid = cells.length === 2 && cells[0] !== cells[1]
          if (isValid) {
            $.tos.playPower(powerId, cells, deferRPC.resolve)
            $.tos.gameboard.swapTiles(cells[0], cells[1])
            $.tos.gameboard.deselect()
            setTimeout(deferResolving.resolve, 1200)
          } else {
            deferRPC.resolve()
            deferResolving.resolve()
          }

          $.when(deferRPC, deferResolving).done(function(results) {
            cb()
            $.tos.input.idle()
            self.idle()
          })

        })
        break;
      case 'flesh_ward':
        var cells = $.tos.gameboard.cellsByType('recovery')
        for (var i = 0; i < cells.length; i++) {
          cells[i].match()
        }
        setTimeout(deferResolving.resolve, 800)
        $.tos.playPower(powerId, null, deferRPC.resolve)

        $.when(deferRPC, deferResolving).done(function(results) {
          if (results.player.health) $.tos.hud.health.modify(results.player.health)
          if (results.player.sanity) $.tos.hud.sanity.modify(results.player.sanity)
          if (results.player.bonus) {
            var msg = '+'+results.player.bonus+' bonus match'+(results.player.bonus === 1 ? '' : 'es')
            $.tos.gameboard.flash(msg)
          }
          $.tos.reseatBoard(results.board, $.tos.input.idle)
        })
        break;
      case 'faustian_riches':
        var cells = $.tos.gameboard.cellsByType('resource')
        for (var i = 0; i < cells.length; i++) {
          cells[i].match()
        }
        setTimeout(deferResolving.resolve, 800)
        $.tos.playPower(powerId, null, deferRPC.resolve)

        $.when(deferRPC, deferResolving).done(function(results) {
          if (results.player.wealth) {
            $.tos.hud.wealth.modify(results.player.wealth)
            $.tos.hud.model.wealth += results.player.wealth
          }
          if (results.player.bonus) {
            var msg = '+'+results.player.bonus+' bonus match'+(results.player.bonus === 1 ? '' : 'es')
            $.tos.gameboard.flash(msg)
          }
          $.tos.reseatBoard(results.board, $.tos.input.idle)
        })
        break;
      case 'ley_line_convergence':
        var cells = $.tos.gameboard.cellsByType('magic')
        for (var i = 0; i < cells.length; i++) {
          cells[i].match()
        }
        setTimeout(deferResolving.resolve, 800)
        $.tos.playPower(powerId, null, deferRPC.resolve)

        $.when(deferRPC, deferResolving).done(function(results) {
          if (results.player.wards) $.tos.hud.wards.modify(results.player.wards)
          if (results.player.magic) $.tos.hud.magic.modify(results.player.magic)
          if (results.player.bonus) {
            var msg = '+'+results.player.bonus+' bonus match'+(results.player.bonus === 1 ? '' : 'es')
            $.tos.gameboard.flash(msg)
          }

          $.tos.reseatBoard(results.board, $.tos.input.idle)
        })
        break;
      case 'chant_of_thoth':
        $.tos.playPower(powerId, null, $.tos.input.idle)
        break;
      case 'the_great_work':
        var cells = $.tos.gameboard.cellsByType(null)
        for (var i = 0; i < cells.length; i++) {
          if (!cells[i].tile.isUpgradable()) cells[i].deactivate()
        }

        $.tos.gameboard.pickerMode(1, function(cells, cb) {
          var index = cells.pop()
          var cell = $.tos.gameboard.cellFromIndex(index)

          cell.puff(deferResolving.resolve)
          $.tos.playPower(powerId, [index], deferRPC.resolve)

          $.when(deferRPC, deferResolving).done(function(results) {
            if (results.transforms) {
              var update = results.transforms.pop()
              cell.tile.tile.removeClass(cell.tile.model.tileId)
              cell.tile.model.tileId = update.tileId
              cell.tile.tile.addClass(update.tileId)
            }
            cb()
            $.tos.input.idle()
            self.idle()
          })
        })
        break;
      case 'body_warping_of_gorgoroth':
        deactivateNonMonsterCells()

        $.tos.gameboard.pickerMode(1, function(cells, cb) {
          var index = cells.pop()
          var cell = $.tos.gameboard.cellFromIndex(index)

          cell.puff(deferResolving.resolve)
          $.tos.playPower(powerId, [index], deferRPC.resolve)

          $.when(deferRPC, deferResolving).done(function(results) {
            if (results.transforms) {
              var update = results.transforms.pop()
              cell.setMonster(update.monster)
            }
            cb()
            $.tos.input.idle()
            self.idle()
          })
        })

        break;
      case 'supernal_binding':
        deactivateNonMonsterCells()

        $.tos.gameboard.pickerMode(1, function(cells, cb) {
          var index = cells.pop()
          var cell = $.tos.gameboard.cellFromIndex(index)

          cell.puff(deferResolving.resolve)
          $.tos.playPower(powerId, [index], deferRPC.resolve)

          $.when(deferRPC, deferResolving).done(function(results) {
            cell.tile.tile.addClass('binding')
            $.tos.gameboard.addEffect('binding', index)
            cb()
            $.tos.input.idle()
            self.idle()
          })

        })
        break;
      case 'call_the_beast':
        deactivateNonMonsterCells()

        $.tos.gameboard.pickerMode(1, function(cells, cb) {
          var index = cells.pop()
          var cell = $.tos.gameboard.cellFromIndex(index)

          cell.tile.tile.addClass('animated short bounce-out-up')
          setTimeout(deferResolving.resolve, 500)
          $.tos.playPower(powerId, [index], deferRPC.resolve)

          $.when(deferRPC, deferResolving).done(function(results) {
            var board = [ [cell.tile.model, 0] ]
            for (var i = cell.y; i > 0; i--) {
              var idx = i * 7
              board.push([i-7, idx])
            }
            
            $.tos.reseatBoard(board, function(){
              cb()
              $.tos.input.idle()
              self.idle()
            })
          })
        })
        break;
      default:
        $.tos.playPower(powerId, null, $.tos.input.idle)
        break;
    }
    $.tos.monsterMods()
    return true
  }
  return self
}