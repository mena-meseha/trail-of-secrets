var Monster = require('/monster').Monster;
var Tile = require('/tile').Tile;
var PlayerInput = require('/input').Player;
var Game = require('/game').Game;
var Dialogs = require('/dialogs/index')
var User = require('/player').User

// global namespace and orchestrator object
var ToS = function() {
  var self = this

  this.input = new PlayerInput()
  this.game = new Game()
  this.monsters = {}

  this.dialogs = Dialogs

  // convinient resolution detection which indirectly uses css media selectors + the JQ :visible selector
  this.isSmallScreen = function() { return $('#show-profile').is(':visible') }

  this.loadUser = function(payload) {
    // override for platform-specific authorization
  }
  this.loadGame = function(game) {
    var isGameSealed = game && game.seals.current === game.seals.total
    self.gameId = game ? game._id : null

    if (!isGameSealed) {
      // initialize various ui components
      self.insanity = $('#profile-insanity table').insanity(game.investigator.insanities);
      self.hud = $('#hud').hud(game.investigator, game.seals, game.turn);
      self.gameboard = $('#gameboard').gameboard(game.cells, game.effects);

      $.tos.monsterMods()
      self.powers = $('#powers').powers(game.investigator.powers);

      self.supplies = game.supplies // store supplies list
    }

    // show advancement dialog if any pending advancements, and game select menu dialog if board is sealed
    var selectingGame = game.gameMenu && game.gameMenu.length > 0
    if (game.advancements && game.advancements.length > 0) {
      $.tos.advancing(game.advancements, function(){
        // if player can select from multiple GOO levels then show selection dialog
        if (selectingGame) {
          $.tos.dialogs.greatOldOnes(game.gameMenu)  
        }
      })
    } else if (selectingGame) {
      $.tos.dialogs.greatOldOnes(game.gameMenu)  
    }    

    // show quickstart tutorial the first time a player vists (based on cookie)
    var tutorialCookieName = 'tos.tutorial'
    if (!$.cookie(tutorialCookieName)) {
      $.tos.dialogs.tutorial()
    }
  }
  this.newGame = function(gooId) {
    var opts = {goo: gooId}
    ss.rpc('game.start', opts, function(err) {
      window.location.reload(false);
    })
  }
  this.startGame = function() {
    var deferWithAuthorization = $.Deferred();

    console.log('starting game')

    if (self.game.hasPlayer()) {
      console.log('has player')
      deferWithAuthorization.resolve({})
    } else {
      var fetchAuthorizationDelay = 1000
      setTimeout( function() {
        console.log('no player, logging into fb')
        $.facebook.login(function(results) {
          // console.log('logged in with '+JSON.stringify(results))
          deferWithAuthorization.resolve(results)
        })
      }, fetchAuthorizationDelay);
    }

    // load game after authorization has been performed
    $.when(deferWithAuthorization).done( function() {
      var opts = {facebook: $.facebook.data, goo: null}
      ss.rpc('game.start', opts, function(err, data, user) {
        if (!$.user) $.user = new User(user)
        self.loadGame(data)
      })
    })
  }
  this.restartGame = function() {
    ss.rpc('game.restart', function(){
      window.location.reload(false);
    })
  }
  this.clearDropAnimations = function() {
    $('.drop-in-1, .drop-in-2, .drop-in-3, .drop-in-4, .drop-in-5, .drop-in-6')
    .removeClass('drop-in-1 drop-in-2 drop-in-3 drop-in-4 drop-in-5 drop-in-6 short animated')
  }
  this.removeScores = function() {
    $('.score.shrink-to').remove()
  }
  this.reseatBoard = function(board, cb) {
    for (var i = 0; i < board.length; i++) {
      var cell_or_index = board[i][0]
      var index = board[i][1]

      if ($.isNumeric(cell_or_index)) {
        self.gameboard.moveTile(cell_or_index, index)
      } else {
        self.gameboard.addTile(index, cell_or_index)
      }
    }

    // clear animation classes after sufficient time has passed
    setTimeout( function(){
      self.clearDropAnimations()
      if (typeof cb === 'function') cb()
    }, 900);
  }

  this.matchResults = function(results, scores) {
    // console.log(JSON.stringify(results))
    
    self.gameboard.stopAnimations()
    $.tos.hud.setTitle()

    $.tos.playTurn(results, function(err, result) {
      if (result === 'victory') {
        // show victory and game selection screens
        $.tos.dialogs.win(scores, results.unlock, function(){
          $.tos.dialogs.greatOldOnes(results.gameMenu)
        })
        $.facebook.score( scores[0].score )
      } else if (result === 'defeat') {
        $.tos.dialogs.lose(scores)
        $.facebook.score( scores[0].score )
      } else {
        self.hud.incrementTurn()
        $.tos.input.idle()
      }
      self.gameboard.clearEffects()
    })
    return
  }

  this.playMatch = function(chain, cb) {
    var deferAnimation = $.Deferred();
    var deferRPC = $.Deferred();
    self.input.resolving()

    // immediately start matched tile animations (to hide latency)
    var minimumAnimationDuration = 800
    for (var i = 0; i < chain.length; i++) {
      $.tos.gameboard.cellFromIndex(chain[i]).match()
    }
    setTimeout( function() {
      $('#gameboard .spin-out').hide() 
      deferAnimation.resolve()
    }, minimumAnimationDuration)

    // start rpc server request
    ss.rpc('play.match', chain, function(err, results, scores) {
      deferRPC.resolve(results, scores)
    });

    // process results once animations and server call are both complete
    $.when(deferRPC, deferAnimation).done(function(results) {
      if (typeof(cb) === "function") cb(results[0], results[1])
      self.removeScores()
    })
  }
  this.playPower = function(powerId, cells, cb) {
    self.input.resolving()
    ss.rpc('play.power', powerId, cells, function(err, results) {
      if (results.cost) {
        self.hud.magic.modify(-results.cost)
      }
      cb(results)
    })
  }
  this.empower = function(powerId, cb) {
    ss.rpc('investigator.empower', powerId, function(err){
      if (typeof(cb) === "function") cb()
    })
  }
  this.supply = function(itemId, usePatronage, cb) {
    self.input.resolving()
    ss.rpc('items.supply', itemId, usePatronage, function(err, results) {
      if (results && results.cost) {
        self.hud.magic.modify(-results.cost)
      }
      cb(results)
    });     
  }
  this.advance = function(advancementId, cb) {
    self.input.resolving()
    ss.rpc('investigator.advance', advancementId, function(err, results) {
      if (!results) return cb(results)

      $.tos.advanced(results)
      cb(results)
    });      
  }
  this.stats = function(cb) {
    ss.rpc('play.stats', function(err, data) {
      cb(data)
    });      
  }
  this.premiums = function(cb) {
    ss.rpc('items.premiumShop', function(err, data) {
      cb(data)
    });      
  }
  this.monster = function(monsterId, cb) {
    // check local cache for monster info
    if (self.monsters[monsterId]) {
      cb(self.monsters[monsterId])
      return
    }

    ss.rpc('monster.info', monsterId, function(err, data) {
      if (data) {
        self.monsters[monsterId] = data // cache monster info

        var chain = self.gameboard.chain 
        var waiting = chain && chain.isWaitingOnMonsterInfo(monsterId)
        if (waiting) cb(data)
      }
    })
  }
}

// initialize global game object
$.tos = new ToS()
$.tos.startGame()