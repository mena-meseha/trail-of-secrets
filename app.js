var CONFIG = require('config');

var http = require('http'),
    ss = require('socketstream');

var isRunningInProduction = ss.env === 'production'

// Use MongoDB as session store
var MongoStore = require('connect-mongo')(ss.http.connect);
ss.session.store.use(new MongoStore(CONFIG.Database.Application));

// Initialize SocketStream clients with site routes and asset loading
require('./server/router').init(ss)

// Code Formatters
ss.client.formatters.add(require('ss-stylus'))

var cdn = ss.env === 'production' ? "https://s3.amazonaws.com/trailofsecrets/static" : null
ss.client.formatters.add(require('./server/ss-cdn/wrapper'), {host: cdn})

// Parse POST requests (e.g. for Facebook callbacks)
ss.http.middleware.prepend( ss.http.connect.bodyParser() )

// Use server-side compiled Hogan (Mustache) templates. Others engines available
ss.client.templateEngine.use(require('ss-hogan'));

// Minimize and pack assets if you type: SS_ENV=production node app.js
if (isRunningInProduction) {
  ss.client.packAssets();
}

// Use separate port in production (to avoid 3G packet filtering over port 80 for non-HTTP traffic) 
var socketPort = ss.env === 'production' ? CONFIG.Server.socketPort : CONFIG.Server.port

ss.ws.transport.use('engineio', {
  client: {
    transports: ['websocket', 'flashsocket', 'xhr-polling', 'jsonp-polling'],
    port: socketPort
  },
  server: function(io) {
    io.set('log level', 1)
    // uncomment to handle mixed-origin server bindings
    // io.set('match origin protocol', true)
    // if (isRunningInProduction) io.set('origins', CONFIG.Server.host+':'+socketPort)  
  }
});

// Initialize Mongoose models
var mongoose = require('mongoose');

// Connect to MongoDB, using authorization in the production env
if (isRunningInProduction) {
  var connString = 'mongodb://'+CONFIG.Database.Application.username+':'+CONFIG.Database.Application.password+'@'+CONFIG.Database.Application.host+':'+CONFIG.Database.Application.port+'/'+CONFIG.Database.Application.db
  mongoose.connect(connString)
} else {
  mongoose.connect(CONFIG.Database.Application.host, CONFIG.Database.Application.db, CONFIG.Database.Application.port);
}

mongoose.model('Power', require('./server/models/power').Power)
mongoose.model('Plot', require('./server/models/plot').Plot)
mongoose.model('Monster', require('./server/models/monster').Monster)
mongoose.model('Tile', require('./server/models/tile').Tile)
mongoose.model('Cell', require('./server/models/cell').Cell)
mongoose.model('Item', require('./server/models/item').Item)
mongoose.model('Insanity', require('./server/models/insanity').Insanity)
mongoose.model('Investigator', require('./server/models/investigator').Investigator)
mongoose.model('Advancement', require('./server/models/advancement').Advancement)
mongoose.model('Score', require('./server/models/score').Score)
mongoose.model('Leaderboard', require('./server/models/leaderboard').Leaderboard)
mongoose.model('User', require('./server/models/user').User)
mongoose.model('GreatOldOne', require('./server/models/great_old_one').GreatOldOne)
mongoose.model('Game', require('./server/models/game').Game)

// Start web server
var server = http.Server(ss.http.middleware);
server.listen( CONFIG.Server.port );

// Start SocketStream (running websockets on same port as web)
ss.start(server)

// update leaderboard rankings (idempotent)
mongoose.model('Leaderboard').rank()